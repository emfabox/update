#!/bin/bash

if [ -f  /var/www/html/license.php ] ; then
	mv -f /var/www/html/license.php /tmp/
fi

if [ -f  /opt/emfa/tmp/license.php ] ; then
	
  mv -f /opt/emfa/tmp/license.php /var/www/html/license.php
  rm -f /tmp/license*.php
  
  chmod 644 /var/www/html/license.php
  
else 
   	mv -f /tmp/license.php /var/www/html/license.php
    chmod 644 /var/www/html/license.php
fi 