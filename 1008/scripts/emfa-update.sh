#!/bin/bash
#----------------------------------------------------------------#
# EMFABox 
#----------------------------------------------------------------#
action="$1"
#----------------------------------------------------------------#
# check variables.conf file

if [ -f  /etc/emfa/variables.conf ] ; then
source  /etc/emfa/variables.conf
else
      echo "Main variables not found"
      exit 0
fi


#----------------------------------------------------------------#
# Variables
#----------------------------------------------------------------#
MIRROR="http://dl.emfabox.org"
VERSIONFILE="/etc/EMFA-Version"
ADMINEMAIL=${EMFABOX_ADMIN_MAIL}
MAILFROM="$ADMINEMAIL"
MAILTO="$ADMINEMAIL"
MAILSUBJECT="New EMFA version available for your host: `hostname`"
SENDMAIL="/usr/lib/sendmail"
TMPMAIL="/tmp/tempmail"
AUTOUPDATES=${EMFABOX_AUTOUPDATE}
#----------------------------------------------------------------#

#----------------------------------------------------------------#
# Function start_update
#----------------------------------------------------------------#
function start_update()
{
  get_cversion
  get_lversion
   if [ "$CVERSION" == "$LVERSION" ]
    then
      echo "You are already running the latest version, no update needed"
      exit 0
    else
      echo "Starting update to $LVERSION"
      cd /opt/emfa/update/
      if [ -f /opt/emfa/update/EMFA-update-script ]
        then
          rm /opt/emfa/update/EMFA-update-script
      fi
      wget -q $MIRROR/update/EMFA-update-script
      chmod 700 EMFA-update-script
      logsave /var/log/emfa/update-`/bin/date +%F-%T`.log /opt/emfa/update/EMFA-update-script
  fi
}
#----------------------------------------------------------------#

#----------------------------------------------------------------#
# Function check_update
#----------------------------------------------------------------#
function check_update()
{
  get_cversion
  echo "Getting latest version number from $MIRROR"
  get_lversion
  if [ "$CVERSION" == "$LVERSION" ]
    then
      echo "You are already running version $LVERSION, no update needed"
      exit 0
    else
      echo "You are running EMFA version $CVERSION"
      echo "Latest version update is $LVERSION"
      exit 0
  fi
}
#----------------------------------------------------------------#
# Function check staging & update dir
#----------------------------------------------------------------#

function check_update_dirs()
{

if [ ! -d /opt/emfa/staging ]; then
mkdir -p /opt/emfa/staging
fi

if [ ! -d /opt/emfa/update ]; then
mkdir -p /opt/emfa/update
fi

}


#----------------------------------------------------------------#
# Function cron_check
#----------------------------------------------------------------#
function cron_check()
{
  get_cversion
  get_lversion

  # Call in our current version number for statistics.
  wget -qO- $MIRROR/versioncheck/`cat /etc/EMFA-Version`.txt $> /dev/null

  if [ "$CVERSION" == "$LVERSION" ]
    then
      exit 0
    else
      if [[ $AUTOUPDATES == "yes" ]]; then
        start_update
      else
	    echo "From: $MAILFROM" > $TMPMAIL
        echo "To: $MAILTO" >> $TMPMAIL
        echo "Reply-To: $MAILFROM" >> $TMPMAIL
        echo "Subject: $MAILSUBJECT" >> $TMPMAIL
        echo "A new update is available for your system" >> $TMPMAIL
        echo "" >> $TMPMAIL
        echo "Currently you are running version $CVERSION the latest version is $LVERSION" >> $TMPMAIL
        echo "" >> $TMPMAIL
        echo "Please visit https://www.emfabox.com for more information." >> $TMPMAIL
        cat $TMPMAIL | $SENDMAIL -t
        rm $TMPMAIL
        exit 0
      fi
  fi
}
#----------------------------------------------------------------#

#----------------------------------------------------------------#
# Function get current version number
#----------------------------------------------------------------#
function get_cversion()
{
  if [ -f /etc/EMFA-Version ]
    then
      CVERSION="`head -1 /etc/EMFA-Version`"
    else
      echo "ERROR: No valid version file found on this system."
      echo "ERROR: exiting now"
      exit 0
  fi

  # Check if we run an beta version
    if [[ $CVERSION =~ ^[0-9]{1}\.[0-9]{1}\.[0-9]{1}\.[0-9]{1}-beta$ ]]
    then
      echo "ERROR: You seem to be running an beta version, no upgrade possible."
      echo "ERROR: please look at https://emfabox.com for more information."
      echo "ERROR: exiting now"
      exit 0
  fi

  # Check if CVERSION is an valid Version file
   if ! [[ $CVERSION =~ ^[0-9]{1}\.[0-9]{1}\.[0-9]{1}\.[0-9]{1}$ ]]
    then
      echo "ERROR: The version file on your system does not seem to be valid."
      echo "ERROR: exiting now"
      exit 0
  fi
}
#----------------------------------------------------------------#

#----------------------------------------------------------------#
# Function get latest version number
#----------------------------------------------------------------#
function get_lversion()
{
  cd /tmp
  if [ -f /tmp/EMFA-Version ]
    then
      rm /tmp/EMFA-Version
  fi

  wget -q $MIRROR/update/EMFA-Version
  if [ -f /tmp/EMFA-Version ]
    then
      LVERSION="`head -1 /tmp/EMFA-Version`"
  fi

  # Check if LVERSION is an valid Version file
  if ! [[ $LVERSION =~ ^[0-9]{1}\.[0-9]{1}\.[0-9]{1}\.[0-9]{1}$ ]]
    then
      echo "ERROR: The version file downloaded does not seem to be valid."
      echo "ERROR: exiting now"
      exit 0
  fi
}
#----------------------------------------------------------------#

#----------------------------------------------------------------#
# Check if we are root
#----------------------------------------------------------------#
function user_check()
{
  if [ `whoami` == root ]
    then
      echo "Good you are root"
      check_update_dirs
      start_update
  else
    echo "Please become root to run this update"
    exit 0
  fi
}
#----------------------------------------------------------------#

#----------------------------------------------------------------#
# show the usage
#----------------------------------------------------------------#
function show_usage()
{
  echo "Usage: $0 [option]"
  echo "Where [option] is:"
  echo ""
  echo "-update"
  echo "   Update to the latest version"
  echo ""
  echo "-check"
  echo "   check if there is a update available"
  echo ""
}
#----------------------------------------------------------------#

#----------------------------------------------------------------#
# Parse action
#----------------------------------------------------------------#
function parse_action()
{
  case $action in
      -update)
        user_check
        ;;
      -check)
        check_update
        ;;
      -cron)
        cron_check
        ;;
      *)
        show_usage
        ;;
  esac
  exit 0
}
#----------------------------------------------------------------#

#----------------------------------------------------------------#
# Main function
#----------------------------------------------------------------#
function main()
{
  if [ "X${action}" == "X" ]
    then
      show_usage
      exit 0
    else
      parse_action
  fi
}
#----------------------------------------------------------------#

#----------------------------------------------------------------#
# Run main
#----------------------------------------------------------------#
main
#----------------------------------------------------------------#