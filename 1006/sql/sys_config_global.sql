-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.1.73-log - Source distribution
-- Server OS:                    redhat-linux-gnu
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table mailscanner.sys_config_global
DROP TABLE IF EXISTS `sys_config_global`;
CREATE TABLE IF NOT EXISTS `sys_config_global` (
  `variable_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mailscanner.sys_config_global: 8 rows
DELETE FROM `sys_config_global`;
/*!40000 ALTER TABLE `sys_config_global` DISABLE KEYS */;
INSERT INTO `sys_config_global` (`variable_name`, `value`) VALUES
	('blacklist_relay_country_score', '1.0'),
	('blacklist_relay_country_codes', 'XA,XB'),
	('whitelist_relay_country_score', '-1.0'),
	('whitelist_relay_country_codes', 'XA,XB'),
	('blacklist_source_country_score', '1.0'),
	('blacklist_source_country_codes', 'XA,XB'),
	('whitelist_source_country_score', '-1.0'),
	('whitelist_source_country_codes', 'XA,XB');
/*!40000 ALTER TABLE `sys_config_global` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
