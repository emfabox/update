#!/bin/bash
########################################################################
# EMFABOX UPGRADE SCRIPT                                               #
#                                                                      #
# V1.a                                                                 #
########################################################################
# Copyright (C) 2014, 2015  http://www.cycomptec.com                   #
########################################################################
#
# Build date 05-09-2015
#
VERSION="1.0.0.3"
OLD_VERSION="1.0.0.2"
logdir="/var/log/emfa"
UPDATEDIR="/tmp/EMFA-Update"

##########################################################################################################################
function emfa_update() {

echo "Starting update to EMFABox $VERSION"

##### Backup Phase #####
#/usr/local/sbin/EMFA-Backup -backup

##### Commit Phase #####

#/usr/local/sbin/emfa

cd /usr/local/sbin/emfa

#check_lsyncd
/usr/bin/wget --no-check-certificate -O /usr/local/sbin/emfa/check_lsyncd  https://bitbucket.org/emfabox/update/raw/0be51c342c71f925de5c39e52632a83950d417ca/1003/scritps/check_lsyncd
chmod 755 /usr/local/sbin/emfa/check_lsyncd

#check_replica
/usr/bin/wget --no-check-certificate -O /usr/local/sbin/emfa/check_replica https://bitbucket.org/emfabox/update/raw/0be51c342c71f925de5c39e52632a83950d417ca/1003/scritps/check_replica
chmod 755 /usr/local/sbin/emfa/check_replica

#run-upgrade.sh
/usr/bin/wget --no-check-certificate -O /usr/local/sbin/run-upgrade.sh https://bitbucket.org/emfabox/update/raw/ca2f960a92fed8cbc9cfc02c621f5483b3882325/1003/scritps/run-upgrade.sh
chmod 755 /usr/local/sbin/run-upgrade.sh

#sudoers.d

cd /etc/sudoers.d

#EMFA-Services
/usr/bin/wget --no-check-certificate -O /etc/sudoers.d/EMFA-Services https://bitbucket.org/emfabox/update/raw/ca2f960a92fed8cbc9cfc02c621f5483b3882325/1003/sudoers.d/EMFA-Services

visudo -c -f /etc/sudoers.d/EMFA-Services > /tmp/visudo



#EMFA-WEB


#copy license 
if [ -f  /var/www/html/license.php ] ; then
	cp -f /var/www/html/license.php /tmp/
fi

#backup old version
cd /var/www/
tar -czvf html-v-${OLD_VERSION}.tar.gz html/*
mv -f html-v-${OLD_VERSION}.tar.gz  /var/www/emfa_backup/

cd /var/www/html/
rm -rf *

cd /opt/emfa/staging/
/usr/bin/wget --no-check-certificate -O /opt/emfa/staging/html.tar.gz https://bitbucket.org/emfabox/update/raw/85850382d6f8d3bad6527ac9b39f80a701d846fd/1003/html.tar.gz

tar -xzvf html.tar.gz
rsync -avz html/* /var/www/html/

rm -rf /opt/emfa/staging/html

#move license back
if [ -f  /tmp/license.php ] ; then
	mv -f /tmp/license.php  /var/www/html/license.php
fi


#remove mailq.js  
if [ -f  /var/www/html/mailq.js ] ; then
	rm -f  /var/www/html/mailq.js
fi


# set permissions
# run script
/usr/local/sbin/set_permissions.sh



}


##########################################################################################################################
### GLOBAL FUNCTIONS 
function emfa_test_host () {

if [ -f /opt/emfa/id/ping_google ] ; then

rm -rf  /opt/emfa/id/ping_google

fi;
touch /opt/emfa/id/ping_google
clear 

echo -----testing host: "$1":"$2" via ping-----
ping -c1 -W1 "$2" | grep 'time=' > /dev/null
#echo $?
if [ $? -eq 0 ]
then
echo OK: "$1" is up
sleep 2s
clear
else
echo -e "\e[1;31mInternet is down or google.com is not accessible so we can not download ... Aborting.\e[0m"; echo;
sleep 2s
exit 1
fi

}

## CHECK OS
function emfa_check_os () {

MACHINE_TYPE=`uname -m`

if [ ${MACHINE_TYPE} == 'x86_64' ]; then

    # 64-bit 
    printf "\n"
    printf "\e[1;42m 64-bit OS detected ...\e[0m";  printf "\n"
    printf "\n"
    
    sleep 2s
    
else 

echo -e "\e[1;31m 32-bit OS detected ... Aborting.\e[0m"; echo;

sleep 2s  
  
  exit 1
  
fi

}

## check MD5 
function emfa_check_md5() {
  # Grab md5 from file
  checksum1=$(cat $UPDATEDIR/$UPDATE.md5 | awk '{print $1}')
  # Calculate md5
  checksum2=$(md5sum $UPDATEDIR/$UPDATE | awk '{print $1}')
  if [[ "$checksum1" != "$checksum2" ]]; then
    echo "Fatal:  $UPDATE md5 checksum does not match!  Aborting..."
    abort
  fi
}

## abort install
function emfa_abort() {
  [[ -n $UPDATEDIR ]] && rm -rf $UPDATEDIR
  
  echo "Incorrect EMFABox version!"
  echo "Update to Version $VERSION FAILED.  Updates cancelled."
  echo "Please visit http://emfabox.com for more information."
  exit 1
}

## finish
function emfa_finish_update() {
  # Write the latest version number
  echo "$VERSION" > /etc/EMFA-Version

  cd /tmp
  [[ -n $UPDATEDIR ]] && rm -rf $UPDATEDIR

  echo "All done"
  echo "If your system does not return to a command prompt"
  echo "you can now safely press ctrl-c to abort logsave"
  exit 0
  exit 0
}

## disk space

function emfa_check_disk_space() {
  # Abort update if disk space is low use is >= 95%
  THRESHOLD="95"
  DISKUSE=$(df -hP -t ext4 --local | awk '{print $5}' | grep [0-9] | tr -d "%")
  for i in $DISKUSE; do
    if [[ $i -ge $THRESHOLD ]]; then

      echo "FATAL:  Update aborted.  Low disk space <=5% detected on one"
      echo "or more local ext4 filesystems!"
      echo "Free up disk space before continuing."
      echo "Please visit http://emfabox.com for assistance."

      abort
    fi
  done

}

# get current version

function emfa_get_cversion() {
  if [ -f /etc/EMFA-Version ]
    then
      CVERSION="`head -1 /etc/EMFA-Version`"
    else
      echo "ERROR: No valid version file found on this system."
      echo "ERROR: exiting now"
      exit 0
  fi

  # Check if we run an beta version
  if [[ $CVERSION =~ ^[0-9]{1}\.[0-9]{1}\.[0-9]{1}\.[0-9]{1}-beta$ ]]
    then
      echo "ERROR: You seem to be running an beta version, no upgrade possible."
      echo "ERROR: please look at http://emfabox.com for more information."
      echo "ERROR: exiting now"
      exit 0
  fi

  # Check if CVERSION is an valid Version file
  if ! [[ $CVERSION =~ ^[0-9]{1}\.[0-9]{1}\.[0-9]{1}\.[0-9]{1}$ ]]
    then
      echo "ERROR: The version file on your system does not seem to be valid."
      echo "ERROR: exiting now"
      exit 0
  fi
}

#initialize 
function emfa_initialize() {

  mkdir -p $UPDATEDIR
  cd $UPDATEDIR
}


function emfa_user_check() {
  if [ `whoami` == root ]
    then
      echo "Good you are root"
  else
    echo "Please become root to run this update"
    exit 0
  fi
}

###
function emfa_reboot() { 

 # Write the latest version number
  echo "$VERSION" > /etc/EMFA-Version

  cd /tmp
  
  rm -f emfaupgrade.sh
  
  [[ -n $UPDATEDIR ]] && rm -rf $UPDATEDIR
  
#Reboot
echo "Done - Rebooting now..."
/sbin/init 6
exit 

}


### STARTUP

function emfa_startup() {
emfa_get_cversion
if [[ $CVERSION == "1.0.0.2" ]] ; then
  emfa_initialize
  emfa_update
#  emfa_finish_update
  emfa_reboot   
else
  echo "ERROR" > /tmp/error
  emfa_abort
fi

}

####################################
# START
####################################
emfa_user_check
emfa_check_os
emfa_check_disk_space
emfa_startup
 





