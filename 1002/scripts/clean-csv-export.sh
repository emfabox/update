#!/bin/bash
#
# remove csv export files 

if [ -d  /var/www/html/master/plugins/jquery-file-upload/server/upload ] ; then

cd /var/www/html/master/plugins/jquery-file-upload/server/upload/

find . -depth -name '*.csv' -mmin +120 -exec rm -rf {} \;

echo "<?php ?>" > /var/www/html/master/plugins/jquery-file-upload/server/upload/email/files/index.html
echo "<?php ?>" > /var/www/html/master/plugins/jquery-file-upload/server/upload/domain/files/index.html
echo "<?php ?>" > /var/www/html/master/plugins/jquery-file-upload/server/upload/relayhosts/files/index.html

chmod 755 /var/www/html/master/plugins/jquery-file-upload/server/upload/email/files
chown apache:apache /var/www/html/master/plugins/jquery-file-upload/server/upload/email/files

chmod 755 /var/www/html/master/plugins/jquery-file-upload/server/upload/domain/files
chown apache:apache /var/www/html/master/plugins/jquery-file-upload/server/upload/domain/files

chmod 755 /var/www/html/master/plugins/jquery-file-upload/server/upload/relayhosts/files
chown apache:apache /var/www/html/master/plugins/jquery-file-upload/server/upload/relayhosts/files

fi 

exit 0