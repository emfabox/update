#!/bin/bash
export from=$1
export to=$2

if [ "$1" = "" ]; then
        echo "usage:"
        echo "cp-helper source destination"
        echo ""
        exit 0
fi


if [ "$2" = "" ]; then
        echo "usage:"
        echo "cp-helper source destination"
        echo ""
        exit 0
fi

/bin/cp -f $from $to