#!/bin/bash
########################################################################
# EMFABOX UPGRADE SCRIPT                                               #
#                                                                      #
# V1.a                                                                 #
########################################################################
# Copyright (C) 2014, 2015  http://www.cycomptec.com                   #
########################################################################
VERSION="1.0.0.2"
logdir="/var/log/emfa"
UPDATEDIR="/tmp/EMFA-Update"



##########################################################################################################################
function emfa_update() {

echo "Starting update to EMFABox $VERSION"

##### Backup Phase #####
#/usr/local/sbin/EMFA-Backup -backup

##### Commit Phase #####

# monit #
yum -y install monit
chkconfig monit on

#/etc/monit.d
cd /etc/monit.d/

#clamd
/usr/bin/wget --no-check-certificate -O  /etc/monit.d/clamd https://bitbucket.org/emfabox/update/raw/cfd3463fef39e91bbb961c7629820797f1f9632d/1002/monit.d/clamd

#crond
/usr/bin/wget --no-check-certificate -O  /etc/monit.d/crond https://bitbucket.org/emfabox/update/raw/cfd3463fef39e91bbb961c7629820797f1f9632d/1002/monit.d/crond

#mysqld
/usr/bin/wget --no-check-certificate -O  /etc/monit.d/mysqld https://bitbucket.org/emfabox/update/raw/cfd3463fef39e91bbb961c7629820797f1f9632d/1002/monit.d/mysqld

#named
/usr/bin/wget --no-check-certificate -O  /etc/monit.d/named https://bitbucket.org/emfabox/update/raw/76afe7eb807a348ee4eaa14e16b239b509ecbf4a/1002/monit.d/named

#postfix-out
/usr/bin/wget --no-check-certificate -O  /etc/monit.d/postfix-out https://bitbucket.org/emfabox/update/raw/cfd3463fef39e91bbb961c7629820797f1f9632d/1002/monit.d/postfix-out

#sendmailanalyzer
/usr/bin/wget --no-check-certificate -O  /etc/monit.d/sendmailanalyzer https://bitbucket.org/emfabox/update/raw/cfd3463fef39e91bbb961c7629820797f1f9632d/1002/monit.d/sendmailanalyzer

# restart monit if allready installed #
/sbin/service monit restart 

# MailScanner #

#Increase sa-learn and spamassassin max message size limits
sed -i "/^Max Spam Check Size =/ c\Max Spam Check Size = 2048k" /etc/MailScanner/MailScanner.conf

#Reply signature behavior
sed -i "/^Dont Sign HTML If Headers Exist =/ c\Dont Sign HTML If Headers Exist = In-Reply-To: References:" /etc/MailScanner/MailScanner.conf

#Disable Notify Senders
sed -i "/^Notify Senders =/ c\Notify Senders = no" /etc/MailScanner/MailScanner.conf


# UPDATE to fit last changes on Mon, 17 Aug 2015 12:36:52 -0400 (12:36 -0400) 

#Web Bug Replacement
sed -i "/^Web Bug Replacement =/ c\Web Bug Replacement = https://s3.amazonaws.com/mailscanner/images/1x1spacer.gif" /etc/MailScanner/MailScanner.conf

#Added support for RBLs that return different ips
#mailscanner/bin/MailScanner/RBLs.pm
#mailscanner/bin/MailScanner/ConfigDefs.pl
#mailscanner/etc/spam.lists.conf

cd /usr/share/MailScanner/MailScanner

#RBLs.pm
/usr/bin/wget --no-check-certificate -O /usr/share/MailScanner/MailScanner/RBLs.pm https://bitbucket.org/emfabox/update/raw/cfd3463fef39e91bbb961c7629820797f1f9632d/1002/MailScanner/RBLs.pm 
chmod 755 /usr/share/MailScanner/MailScanner/RBLs.pm

#ConfigDefs.pl
/usr/bin/wget --no-check-certificate -O /usr/share/MailScanner/MailScanner/ConfigDefs.pl https://bitbucket.org/emfabox/update/raw/cfd3463fef39e91bbb961c7629820797f1f9632d/1002/MailScanner/ConfigDefs.pl
chmod 755 /usr/share/MailScanner/MailScanner/ConfigDefs.pl

cd /etc/MailScanner

#spam.lists.conf
/usr/bin/wget --no-check-certificate -O /etc/MailScanner/spam.lists.conf https://bitbucket.org/emfabox/update/raw/76afe7eb807a348ee4eaa14e16b239b509ecbf4a/1002/MailScanner/spam.lists.conf

cd /usr/sbin

#update_bad_phishing_emails
/usr/bin/wget --no-check-certificate -O /usr/sbin/update_bad_phishing_emails https://bitbucket.org/emfabox/update/raw/cfd3463fef39e91bbb961c7629820797f1f9632d/1002/MailScanner/update_bad_phishing_emails
chmod 755 /usr/sbin/update_bad_phishing_emails

if [ ! -d  /var/cache/ScamNailer ] ; then
	mkdir -p /var/cache/ScamNailer/
fi
  
if [ ! -f  /etc/mail/spamassassin/ScamNailer.cf ] ; then
	touch /etc/mail/spamassassin/ScamNailer.cf
fi

#run update 
/usr/sbin/update_bad_phishing_emails

# cron.d

cd /etc/cron.d

#emfa
/usr/bin/wget --no-check-certificate -O /etc/cron.d/emfa  https://bitbucket.org/emfabox/update/raw/22c9c058f742d3a9061c3d2ce10d2f5aa5169115/1002/cron.d/emfa.cron
chmod 0640 /etc/cron.d/*

#cron.daily
cd /etc/cron.daily/
rm -f /etc/cron.daily/reloadhttpd
chmod 755 /etc/cron.daily/*

#cron.hourly
chmod 755 /etc/cron.hourly/*

#cron.weekly
chmod 755 /etc/cron.weekly/*

#cron.monthly
chmod 755 /etc/cron.monthly/*


# scripts #
cd /usr/local/sbin/emfa

#check_avira.sh
/usr/bin/wget --no-check-certificate -O /usr/local/sbin/emfa/check_avira.sh https://bitbucket.org/emfabox/update/raw/ca99500477783bbff750d5806ca4e275452d57e4/1002/scripts/check_avira.sh
chmod 755 /usr/local/sbin/emfa/check_avira.sh

cd /usr/local/sbin
/usr/bin/wget --no-check-certificate -O /usr/local/sbin/set_permissions.sh https://bitbucket.org/emfabox/update/raw/ca99500477783bbff750d5806ca4e275452d57e4/1002/scripts/set_permissions.sh
chmod 755 /usr/local/sbin/set_permissions.sh

#set-update.sh
/usr/bin/wget --no-check-certificate -O /usr/local/sbin/set-update.sh https://bitbucket.org/emfabox/update/raw/2fd41518f3a14948a5dffec28642b884d2b2ed53/1002/scripts/set-update.sh
chmod 755 /usr/local/sbin/set-update.sh

#clean-csv-export.sh
/usr/bin/wget --no-check-certificate -O /usr/local/sbin/clean-csv-export.sh https://bitbucket.org/emfabox/update/raw/5b715d9327625d00baa957f1bc14db67941edea9/1002/scripts/clean-csv-export.sh
chmod 755 /usr/local/sbin/clean-csv-export.sh

#cp-helper.sh
/usr/bin/wget --no-check-certificate -O /usr/local/sbin/cp-helper.sh https://bitbucket.org/emfabox/update/raw/13c2e449e5671e99c20064fd16039fea2e09d084/1002/scripts/cp-helper.sh
chmod 755 /usr/local/sbin/cp-helper.sh

#mv-helper.sh
/usr/bin/wget --no-check-certificate -O /usr/local/sbin/mv-helper.sh https://bitbucket.org/emfabox/update/raw/13c2e449e5671e99c20064fd16039fea2e09d084/1002/scripts/mv-helper.sh
chmod 755 /usr/local/sbin/mv-helper.sh

#rm-helper.sh
/usr/bin/wget --no-check-certificate -O /usr/local/sbin/rm-helper.sh https://bitbucket.org/emfabox/update/raw/13c2e449e5671e99c20064fd16039fea2e09d084/1002/scripts/rm-helper.sh
chmod 755 /usr/local/sbin/rm-helper.sh 

#sudoers.d

cd /etc/sudoers.d

#EMFA-Services
/usr/bin/wget --no-check-certificate -O /etc/sudoers.d/EMFA-Services https://bitbucket.org/emfabox/update/raw/052ce0acbfe6ab6feffd10982e3f545de5391bb4/1002/sudoers.d/EMFA-Services

#EMFA-WEB

cd /opt/emfa/staging/
/usr/bin/wget --no-check-certificate -O /opt/emfa/staging/html.tar.gz https://bitbucket.org/emfabox/update/src/859bf15a0215b42ce2a589e2a3cf2c3164e4f04a/1002/html.tar.gz?at=master

tar -xzvf html.tar.gz

rsync -avz html/* /var/www/html/

rm -rf /opt/emfa/staging/html

#remove mailq.js  
if [ -f  /var/www/html/mailq.js ] ; then
	rm -f  /var/www/html/mailq.js
fi


# set permissions
# run script
/usr/local/sbin/set_permissions.sh



}


##########################################################################################################################
### GLOBAL FUNCTIONS 
function emfa_test_host () {

if [ -f /opt/emfa/id/ping_google ] ; then

rm -rf  /opt/emfa/id/ping_google

fi;
touch /opt/emfa/id/ping_google
clear 

echo -----testing host: "$1":"$2" via ping-----
ping -c1 -W1 "$2" | grep 'time=' > /dev/null
#echo $?
if [ $? -eq 0 ]
then
echo OK: "$1" is up
sleep 2s
clear
else
echo -e "\e[1;31mInternet is down or google.com is not accessible so we can not download ... Aborting.\e[0m"; echo;
sleep 2s
exit 1
fi

}

## CHECK OS
function emfa_check_os () {

MACHINE_TYPE=`uname -m`

if [ ${MACHINE_TYPE} == 'x86_64' ]; then

    # 64-bit 
    printf "\n"
    printf "\e[1;42m 64-bit OS detected ...\e[0m";  printf "\n"
    printf "\n"
    
    sleep 2s
    
else 

echo -e "\e[1;31m 32-bit OS detected ... Aborting.\e[0m"; echo;

sleep 2s  
  
  exit 1
  
fi

}

## check MD5 
function emfa_check_md5() {
  # Grab md5 from file
  checksum1=$(cat $UPDATEDIR/$UPDATE.md5 | awk '{print $1}')
  # Calculate md5
  checksum2=$(md5sum $UPDATEDIR/$UPDATE | awk '{print $1}')
  if [[ "$checksum1" != "$checksum2" ]]; then
    echo "Fatal:  $UPDATE md5 checksum does not match!  Aborting..."
    abort
  fi
}

## abort install
function emfa_abort() {
  [[ -n $UPDATEDIR ]] && rm -rf $UPDATEDIR
  
  echo "Incorrect EMFABox version!"
  echo "Update to Version $VERSION FAILED.  Updates cancelled."
  echo "Please visit http://emfabox.com for more information."
  exit 1
}

## finish
function emfa_finish_update() {
  # Write the latest version number
  echo "$VERSION" > /etc/EMFA-Version

  cd /tmp
  [[ -n $UPDATEDIR ]] && rm -rf $UPDATEDIR

  echo "All done"
  echo "If your system does not return to a command prompt"
  echo "you can now safely press ctrl-c to abort logsave"
  exit 0
  exit 0
}

## disk space

function emfa_check_disk_space() {
  # Abort update if disk space is low use is >= 95%
  THRESHOLD="95"
  DISKUSE=$(df -hP -t ext4 --local | awk '{print $5}' | grep [0-9] | tr -d "%")
  for i in $DISKUSE; do
    if [[ $i -ge $THRESHOLD ]]; then

      echo "FATAL:  Update aborted.  Low disk space <=5% detected on one"
      echo "or more local ext4 filesystems!"
      echo "Free up disk space before continuing."
      echo "Please visit http://emfabox.com for assistance."

      abort
    fi
  done

}

# get current version

function emfa_get_cversion() {
  if [ -f /etc/EMFA-Version ]
    then
      CVERSION="`head -1 /etc/EMFA-Version`"
    else
      echo "ERROR: No valid version file found on this system."
      echo "ERROR: exiting now"
      exit 0
  fi

  # Check if we run an beta version
  if [[ $CVERSION =~ ^[0-9]{1}\.[0-9]{1}\.[0-9]{1}\.[0-9]{1}-beta$ ]]
    then
      echo "ERROR: You seem to be running an beta version, no upgrade possible."
      echo "ERROR: please look at http://emfabox.com for more information."
      echo "ERROR: exiting now"
      exit 0
  fi

  # Check if CVERSION is an valid Version file
  if ! [[ $CVERSION =~ ^[0-9]{1}\.[0-9]{1}\.[0-9]{1}\.[0-9]{1}$ ]]
    then
      echo "ERROR: The version file on your system does not seem to be valid."
      echo "ERROR: exiting now"
      exit 0
  fi
}

#initialize 
function emfa_initialize() {

  mkdir -p $UPDATEDIR
  cd $UPDATEDIR
}


function emfa_user_check() {
  if [ `whoami` == root ]
    then
      echo "Good you are root"
  else
    echo "Please become root to run this update"
    exit 0
  fi
}

###
function emfa_reboot() { 

 # Write the latest version number
  echo "$VERSION" > /etc/EMFA-Version

  cd /tmp
  
  rm -f emfaupgrade.sh
  
  [[ -n $UPDATEDIR ]] && rm -rf $UPDATEDIR
  
#Reboot
echo "Done - Rebooting now..."
/sbin/init 6
exit 

}


### STARTUP

function emfa_startup() {
emfa_get_cversion
if [[ $CVERSION == "1.0.0.1" ]] ; then
  emfa_initialize
  emfa_update
#  emfa_finish_update
  emfa_reboot
elif [[ $CVERSION == "1.0.0.2" ]] ; then
  emfa_initialize
  emfa_update
#  emfa_finish_update
  emfa_reboot   
else
  echo "ERROR" > /tmp/error
  emfa_abort
fi

}

####################################
# START
####################################
emfa_user_check
emfa_check_os
emfa_check_disk_space
emfa_startup
 





