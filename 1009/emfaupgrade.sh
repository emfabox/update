#!/bin/bash
########################################################################
# EMFABOX UPGRADE SCRIPT                                               #
#                                                                      #
# V1.a                                                                 #
########################################################################
# Copyright (C) 2014, 2015  http://www.cycomptec.com                   #
########################################################################
#
# Build date 15-10-2015
#
VERSION="1.0.0.9"
OLD_VERSION="1.0.0.8"
OLD_BACKUP="1.0.0.7"
logdir="/var/log/emfa"
UPDATEDIR="/tmp/EMFA-Update"

##########################################################################################################################
function emfa_update() {

echo "Starting update to EMFABox $VERSION"

##### Backup Phase #####
#/usr/local/sbin/EMFA-Backup -backup

##### Commit Phase #####


#EMFABOX_AUTOUPDATE="no"
sed -i '/^EMFABOX_MEMORY/a EMFABOX_AUTOUPDATE="no"' /etc/emfa/variables.conf

#update dirs
if [ ! -d /opt/emfa/update ] ; then
    mkdir -p /opt/emfa/update
fi

if [ ! -d /opt/emfa/staging ] ; then
    mkdir -p /opt/emfa/staging
fi

# update centos 
yum update -y

# typo postfix-out

postconf -c /etc/postfix-out/ -e 'syslog_name = postfix-out'

#Web Bug Replacement
sed -i "/^Web Bug Replacement =/ c\Web Bug Replacement = https://s3.amazonaws.com/mailscanner/images/1x1spacer.gif" /etc/MailScanner/MailScanner.conf

#cron.d
cd /etc/cron.d

#https://bitbucket.org/emfabox/update/raw/1dfe6ddf4983fca0fe8f3324621617993b9cc5f9/1009/etc/cron.d/emfa
/usr/bin/wget --no-check-certificate -O /etc/cron.d/emfa https://bitbucket.org/emfabox/update/raw/1dfe6ddf4983fca0fe8f3324621617993b9cc5f9/1009/etc/cron.d/emfa
chmod 0644 /etc/cron.d/emfa

# remove emfa-clean-sendmailanalyzer
rm -f /etc/cron.weekly/emfa-clean-sendmailanalyzer


cd /etc/cron.daily
#https://bitbucket.org/emfabox/update/raw/f4f646ec35c3698a6a95f88d0b88e45e628aac67/1009/etc/cron.daily/EMFA-Daily-cron
/usr/bin/wget --no-check-certificate -O /etc/cron.daily/EMFA-Daily-cron https://bitbucket.org/emfabox/update/raw/f4f646ec35c3698a6a95f88d0b88e45e628aac67/1009/etc/cron.daily/EMFA-Daily-cron
chmod 755 EMFA-Daily-cron

cd /etc/cron.hourly

#https://bitbucket.org/emfabox/update/raw/663a41da6e787e8e601a4258d837fb60f7e0f96e/1009/etc/cron.hourly/clean-csv-export
/usr/bin/wget --no-check-certificate -O /etc/cron.hourly/clean-csv-export https://bitbucket.org/emfabox/update/raw/663a41da6e787e8e601a4258d837fb60f7e0f96e/1009/etc/cron.hourly/clean-csv-export
chmod 755 clean-csv-export

#https://bitbucket.org/emfabox/update/raw/1dfe6ddf4983fca0fe8f3324621617993b9cc5f9/1009/etc/cron.hourly/EMFA-Hourly-cron
/usr/bin/wget --no-check-certificate -O /etc/cron.hourly/EMFA-Hourly-cron https://bitbucket.org/emfabox/update/raw/1dfe6ddf4983fca0fe8f3324621617993b9cc5f9/1009/etc/cron.hourly/EMFA-Hourly-cron
chmod 755 EMFA-Hourly-cron

cd /usr/local/sbin/emfa/

#https://bitbucket.org/emfabox/update/raw/663a41da6e787e8e601a4258d837fb60f7e0f96e/1009/scripts/usr/local/sbin/emfa/clean-csv-export.sh
/usr/bin/wget --no-check-certificate -O clean-csv-export.sh https://bitbucket.org/emfabox/update/raw/663a41da6e787e8e601a4258d837fb60f7e0f96e/1009/scripts/usr/local/sbin/emfa/clean-csv-export.sh
chmod 755 clean-csv-export.sh

#https://bitbucket.org/emfabox/update/raw/7c18a231c9bc8378dbf31e0520341573f9ab073a/1009/scripts/usr/local/sbin/emfa/merge_phishing_bad
/usr/bin/wget --no-check-certificate -O merge_phishing_bad https://bitbucket.org/emfabox/update/raw/7c18a231c9bc8378dbf31e0520341573f9ab073a/1009/scripts/usr/local/sbin/emfa/merge_phishing_bad
chmod 755 merge_phishing_bad

#https://bitbucket.org/emfabox/update/raw/1dfe6ddf4983fca0fe8f3324621617993b9cc5f9/1009/scripts/usr/local/sbin/emfa/merge_phishing_safe
/usr/bin/wget --no-check-certificate -O merge_phishing_safe https://bitbucket.org/emfabox/update/raw/1dfe6ddf4983fca0fe8f3324621617993b9cc5f9/1009/scripts/usr/local/sbin/emfa/merge_phishing_safe
chmod 755 merge_phishing_safe

cd /usr/sbin

#https://bitbucket.org/emfabox/update/raw/ddcb6407c355724a65f040d5df6f750db2ccacf3/1009/scripts/usr/sbin/update_bad_phishing_sites
/usr/bin/wget --no-check-certificate -O update_bad_phishing_sites https://bitbucket.org/emfabox/update/raw/ddcb6407c355724a65f040d5df6f750db2ccacf3/1009/scripts/usr/sbin/update_bad_phishing_sites
chmod 755 update_bad_phishing_sites

#https://bitbucket.org/emfabox/update/raw/c765e3f07823ab677581f060728491b4224b3f93/1009/scripts/usr/sbin/update_phishing_sites
/usr/bin/wget --no-check-certificate -O update_phishing_sites  https://bitbucket.org/emfabox/update/raw/c765e3f07823ab677581f060728491b4224b3f93/1009/scripts/usr/sbin/update_phishing_sites
chmod 755 update_phishing_sites

cd /usr/local/sbin


#https://bitbucket.org/emfabox/update/raw/8fdbaa8f0da11dd82882ba42ea1472a165dffbd9/1009/scripts/usr/local/sbin/EMFA-MS-Update
/usr/bin/wget --no-check-certificate -O EMFA-MS-Update https://bitbucket.org/emfabox/update/raw/8fdbaa8f0da11dd82882ba42ea1472a165dffbd9/1009/scripts/usr/local/sbin/EMFA-MS-Update
chmod 755 EMFA-MS-Update

#sudoers.d

cd /etc/sudoers.d

#EMFA-Services
#https://bitbucket.org/emfabox/update/raw/cdc79cde3efe3fe188399c7cbb07894197087d4f/1009/sudoers.d/EMFA-Services
/usr/bin/wget --no-check-certificate -O /etc/sudoers.d/EMFA-Services https://bitbucket.org/emfabox/update/raw/cdc79cde3efe3fe188399c7cbb07894197087d4f/1009/sudoers.d/EMFA-Services

visudo -c -f /etc/sudoers.d/EMFA-Services > /tmp/visudo


###  X-Frame-Options

#if [ ! -f /etc/httpd/conf.d/headers.conf ] ; then

printf "<IfModule mod_headers.c>\n" > /etc/httpd/conf.d/headers.conf
printf "    Header unset ETag\n" >> /etc/httpd/conf.d/headers.conf
printf "    Header set X-Frame-Options: SAMEORIGIN\n" >> /etc/httpd/conf.d/headers.conf
printf '    Header set X-XSS-Protection: "1; mode=block"\n' >> /etc/httpd/conf.d/headers.conf
printf "    Header set X-Content-Type-Options: nosniff\n" >> /etc/httpd/conf.d/headers.conf
printf "    Header set X-WebKit-CSP: \"default-src 'self'\"\n" >> /etc/httpd/conf.d/headers.conf
printf '    Header set X-Permitted-Cross-Domain-Policies: "master-only"\n' >> /etc/httpd/conf.d/headers.conf
printf "</IfModule>\n" >> /etc/httpd/conf.d/headers.conf

#fi


##


# sql
MYSQLROOTPWD=`grep MYSQLROOTPWD /etc/DB-Config | sed 's/.*://'`
now=$(date +"%m_%d_%Y")
mysqldump -u root -p"${MYSQLROOTPWD}" mailscanner > /var/www/emfa_backup/${OLD_VERSION}_$now.sql
cd  /var/www/emfa_backup/
tar -czvf  ${OLD_VERSION}_$now.sql.tar.gz ${OLD_VERSION}_$now.sql
rm -f  /var/www/emfa_backup/*.sql

cd /opt/emfa/msdbconf/

#https://bitbucket.org/emfabox/update/raw/7681b65577ae6ddc1426c5c69c6129ffbe32300b/1009/sql/geoip_country.sql
/usr/bin/wget --no-check-certificate -O /opt/emfa/msdbconf/geoip_country.sql https://bitbucket.org/emfabox/update/raw/7681b65577ae6ddc1426c5c69c6129ffbe32300b/1009/sql/geoip_country.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/geoip_country.sql

#https://bitbucket.org/emfabox/update/raw/aee22689d7952ae3c3c04e3a426c027b58f50536/1009/sql/sys_dob_lookup.sql
/usr/bin/wget --no-check-certificate -O /opt/emfa/msdbconf/sys_dob_lookup.sql https://bitbucket.org/emfabox/update/raw/aee22689d7952ae3c3c04e3a426c027b58f50536/1009/sql/sys_dob_lookup.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/sys_dob_lookup.sql

#https://bitbucket.org/emfabox/update/raw/be390415c60bbf847faf427ed84387c94cc8d603/1009/sql/sys_iprep_domain_data.sql
/usr/bin/wget --no-check-certificate -O /opt/emfa/msdbconf/sys_iprep_domain_data.sql https://bitbucket.org/emfabox/update/raw/be390415c60bbf847faf427ed84387c94cc8d603/1009/sql/sys_iprep_domain_data.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/sys_iprep_domain_data.sql

#https://bitbucket.org/emfabox/update/raw/f3f7507d87b6e2ed38c54e09bf34bd8d102078e4/1009/sql/sys_iprep_wl_data.sql
/usr/bin/wget --no-check-certificate -O /opt/emfa/msdbconf/sys_iprep_wl_data.sql https://bitbucket.org/emfabox/update/raw/f3f7507d87b6e2ed38c54e09bf34bd8d102078e4/1009/sql/sys_iprep_wl_data.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/sys_iprep_wl_data.sql

#https://bitbucket.org/emfabox/update/raw/f3f7507d87b6e2ed38c54e09bf34bd8d102078e4/1009/sql/sys_iprep_data_source.sql
/usr/bin/wget --no-check-certificate -O /opt/emfa/msdbconf/sys_iprep_data_source.sql https://bitbucket.org/emfabox/update/raw/f3f7507d87b6e2ed38c54e09bf34bd8d102078e4/1009/sql/sys_iprep_data_source.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/sys_iprep_data_source.sql

#https://bitbucket.org/emfabox/update/raw/4d958f3ea36d1573684f9ba7bcc8e51c00bd15fc/1009/sql/sys_iprep_data.sql
/usr/bin/wget --no-check-certificate -O /opt/emfa/msdbconf/sys_iprep_data.sql https://bitbucket.org/emfabox/update/raw/4d958f3ea36d1573684f9ba7bcc8e51c00bd15fc/1009/sql/sys_iprep_data.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/sys_iprep_data.sql

#https://bitbucket.org/emfabox/update/raw/2ef3a615c3ade154da22746c3ee3988a92d53f90/1009/sql/sys_mail_log.sql
/usr/bin/wget --no-check-certificate -O /opt/emfa/msdbconf/sys_mail_log.sql https://bitbucket.org/emfabox/update/raw/2ef3a615c3ade154da22746c3ee3988a92d53f90/1009/sql/sys_mail_log.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/sys_mail_log.sql

#https://bitbucket.org/emfabox/update/raw/34a3ed1c97885db71ac76eecf6bdd3a839b3e14b/1009/sql/sys_config_global.sql
/usr/bin/wget --no-check-certificate -O /opt/emfa/msdbconf/sys_config_global.sql https://bitbucket.org/emfabox/update/raw/34a3ed1c97885db71ac76eecf6bdd3a839b3e14b/1009/sql/sys_config_global.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/sys_config_global.sql

#https://bitbucket.org/emfabox/update/raw/c021eb94f9bd257773db20dfe160279843fb8f18/1009/sql/slog.sql
/usr/bin/wget --no-check-certificate -O /opt/emfa/msdbconf/slog.sql https://bitbucket.org/emfabox/update/raw/c021eb94f9bd257773db20dfe160279843fb8f18/1009/sql/slog.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/slog.sql

#https://bitbucket.org/emfabox/update/raw/54af57ddf008de1c83ec27cf7dfedbf27c454f96/1009/sql/sys_blacklist_lookup_email.sql
/usr/bin/wget --no-check-certificate -O /opt/emfa/msdbconf/sys_blacklist_lookup_email.sql https://bitbucket.org/emfabox/update/raw/54af57ddf008de1c83ec27cf7dfedbf27c454f96/1009/sql/sys_blacklist_lookup_email.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/sys_blacklist_lookup_email.sql

#https://bitbucket.org/emfabox/update/raw/1f3206866411e7eb2168cae3c1de8f672d87286a/1009/sql/sys_blacklist_lookup_country.sql
/usr/bin/wget --no-check-certificate -O /opt/emfa/msdbconf/sys_blacklist_lookup_country.sql https://bitbucket.org/emfabox/update/raw/1f3206866411e7eb2168cae3c1de8f672d87286a/1009/sql/sys_blacklist_lookup_country.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/sys_blacklist_lookup_country.sql


#https://bitbucket.org/emfabox/update/raw/65e862a657d69fca6c7af2a8dd6140ef95dc781b/1009/sql/sys_phishing_sites.sql
/usr/bin/wget --no-check-certificate -O /opt/emfa/msdbconf/sys_phishing_sites.sql https://bitbucket.org/emfabox/update/raw/65e862a657d69fca6c7af2a8dd6140ef95dc781b/1009/sql/sys_phishing_sites.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/sys_phishing_sites.sql

#https://bitbucket.org/emfabox/update/raw/fa8e746c53ec213a534370557a9db7f1b40c29b3/1009/sql/lusers.sql
/usr/bin/wget --no-check-certificate -O /opt/emfa/msdbconf/lusers.sql https://bitbucket.org/emfabox/update/raw/fa8e746c53ec213a534370557a9db7f1b40c29b3/1009/sql/lusers.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/lusers.sql

#https://bitbucket.org/emfabox/update/raw/35379b22a86192acca90b5ace3b503113f6edb2c/1009/sql/sys_rlog_config.sql
/usr/bin/wget --no-check-certificate -O /opt/emfa/msdbconf/sys_rlog_config.sql https://bitbucket.org/emfabox/update/raw/35379b22a86192acca90b5ace3b503113f6edb2c/1009/sql/sys_rlog_config.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/sys_rlog_config.sql

#https://bitbucket.org/emfabox/update/raw/cdc79cde3efe3fe188399c7cbb07894197087d4f/1009/sql/sys_recover_password.sql
/usr/bin/wget --no-check-certificate -O /opt/emfa/msdbconf/sys_recover_password.sql https://bitbucket.org/emfabox/update/raw/cdc79cde3efe3fe188399c7cbb07894197087d4f/1009/sql/sys_recover_password.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/sys_recover_password.sql


# ALTER TABLE `sys_dob_lookup` ADD COLUMN `hostname` TEXT NULL COLLATE 'utf8_unicode_ci' AFTER `sid`

echo "ALTER TABLE \`sys_dob_lookup\` ADD COLUMN \`hostname\` TEXT NULL COLLATE 'utf8_unicode_ci' AFTER \`sid\`;">/opt/emfa/msdbconf/sys_dob_lookup.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/sys_dob_lookup.sql

echo "ALTER TABLE \`sys_dob_lookup\` ADD COLUMN \`lscore\` TEXT NULL COLLATE 'utf8_unicode_ci' AFTER \`score\`;">/opt/emfa/msdbconf/sys_dob_lookup.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/sys_dob_lookup.sql

echo "INSERT INTO \`sys_rbl_config\` VALUES ('mxscore', '5');">/opt/emfa/msdbconf/sys_rbl_config
echo "INSERT INTO \`sys_rbl_config\` VALUES ('rblscore', '5');">>/opt/emfa/msdbconf/sys_rbl_config
echo "INSERT INTO \`sys_rbl_config\` VALUES ('spfscore', '0');">>/opt/emfa/msdbconf/sys_rbl_config
echo "INSERT INTO \`sys_rbl_config\` VALUES ('sbase', '5');">>/opt/emfa/msdbconf/sys_rbl_config
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/sys_rbl_config

## check and repair databases  

mysqlcheck -hlocalhost -uroot -p"${MYSQLROOTPWD}" --repair --all-databases


################################################################################
#/etc/php.d/suhosin.ini

if [ -f /etc/php.d/suhosin.ini ] ; then

printf "; Enable suhosin extension module\n" > /etc/php.d/suhosin.ini
printf "extension=suhosin.so\n" >> /etc/php.d/suhosin.ini
printf "suhosin.memory_limit=2048M\n" >> /etc/php.d/suhosin.ini

fi


if [ -f /var/www/icons/README ] ; then
rm -f /var/www/icons/README
fi

if [ -f /var/www/icons/README.html ] ; then
rm -f /var/www/icons/README.html
fi

echo " ">/var/www/icons/index.html









################################################################################

sleep 10s


####
#  rbldnsd
###
cd /etc/init.d/
#https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/init.d/rbldnsd
/usr/bin/wget --no-check-certificate -O /etc/init.d/rbldnsd https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/init.d/rbldnsd
chmod 755  /etc/init.d/rbldnsd

chkconfig --add rbldnsd
chkconfig rbldnsd on


##
cd /usr/local/sbin/
#https://bitbucket.org/emfabox/update/raw/7d9d1b1c0bed7692c4e344710e50a692224c5760/1009/scripts/usr/local/sbin/rbldnsd.sh
/usr/bin/wget --no-check-certificate -O /usr/local/sbin/rbldnsd.sh https://bitbucket.org/emfabox/update/raw/7d9d1b1c0bed7692c4e344710e50a692224c5760/1009/scripts/usr/local/sbin/rbldnsd.sh
chmod 755 rbldnsd.sh

./rbldnsd.sh create


#https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/sysconfig/rbldnsd

cd /etc/sysconfig
/usr/bin/wget --no-check-certificate -O /etc/sysconfig/rbldnsd  https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/sysconfig/rbldnsd

service rbldnsd restart

##################
# remove  sendmailanalyzer
###
cd /etc/init.d/

service sendmailanalyzer stop
chkconfig sendmailanalyzer off
chkconfig --del sendmailanalyzer

cd /etc/logrotate.d

#https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/logrotate.d/syslog

/usr/bin/wget --no-check-certificate -O /etc/logrotate.d/syslog  https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/logrotate.d/syslog

###

cd /etc/monit.d

rm -f sendmailanalyzer

service monit restart

#####
#  spamassassin 
#####

cd /etc/mail/spamassassin/

#10_traps.cf
#https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/10_traps.cf
/usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/10_traps.cf https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/10_traps.cf

#20_mspike.cf
#https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/20_mspike.cf
/usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/20_mspike.cf https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/20_mspike.cf

#mime_validate.cf
#https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/mime_validate.cf
/usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/mime_validate.cf https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/mime_validate.cf

#msonline.cf
#https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/msonline.cf
/usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/msonline.cf https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/msonline.cf

#tls_auth.cf
#https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/tls_auth.cf
/usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/tls_auth.cf https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/tls_auth.cf

#urltemplate.cf
#https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/urltemplate.cf
/usr/bin/wget --no-check-certificate -O /etc/mail/spamassassin/urltemplate.cf https://bitbucket.org/emfabox/update/raw/d7f8a3a3ab8b48c9616bc341bd412960249c9e59/1009/etc/mail/spamassassin/urltemplate.cf

### named

cd /etc/named/
#https://bitbucket.org/emfabox/update/raw/3708627ff32c2a583d83a4790b578fd5971be844/1009/etc/named/ip.pools

/usr/bin/wget --no-check-certificate -O /etc/named/ip.pools https://bitbucket.org/emfabox/update/raw/3708627ff32c2a583d83a4790b578fd5971be844/1009/etc/named/ip.pools

#copy license 
if [ -f  /var/www/html/license.php ] ; then
	cp -f /var/www/html/license.php /tmp/
fi

# remove old backup
if [ -f  /var/www/emfa_backup/html-v-${OLD_BACKUP}.tar.gz ] ; then
	rm -f /var/www/emfa_backup/html-v-${OLD_BACKUP}.tar.gz
fi


#backup old version
cd /var/www/
tar -czvf html-v-${OLD_VERSION}.tar.gz html/*
mv -f html-v-${OLD_VERSION}.tar.gz  /var/www/emfa_backup/

cd /var/www/html/
rm -rf *

cd /opt/emfa/staging/
/usr/bin/wget --no-check-certificate -O /opt/emfa/staging/html.tar.gz https://s3-us-west-2.amazonaws.com/emfabox/html/html.tar.gz

tar -xzvf html.tar.gz
rsync -avz html/* /var/www/html/

rm -rf /opt/emfa/staging/html

# set permissions
# run script
/usr/local/sbin/set_permissions.sh

#https://s3-us-west-2.amazonaws.com/emfabox/demo/license.php
/usr/bin/wget --no-check-certificate -O /var/www/html/license.php https://s3-us-west-2.amazonaws.com/emfabox/demo/license.php

# run dns !!!
php /var/www/html/master/sbin/emfa-gendns.php generate


#move license back
if [ -f  /tmp/license.php ] ; then
	mv -f /tmp/license.php  /var/www/html/license.php
fi

# run dns !!!
php /var/www/html/master/sbin/emfa-gendns.php generate


### 

echo "nameserver 127.0.0.1" > /etc/resolv.conf
echo "nameserver 8.8.8.8" >> /etc/resolv.conf
echo "nameserver 8.8.4.4" >> /etc/resolv.conf

#Disable dhclient-script from updating resolv.conf

cat <<END > /etc/dhclient-enter-hooks
#!/bin/sh
make_resolv_conf(){
        :
}

END

chmod a+x /etc/dhclient-enter-hooks

## to be sure --> 
echo 'PEERDNS="no"'  >> /etc/sysconfig/network-scripts/ifcfg-eth0



}


##########################################################################################################################
### GLOBAL FUNCTIONS 
function emfa_test_host () {

if [ -f /opt/emfa/id/ping_google ] ; then

rm -rf  /opt/emfa/id/ping_google

fi;
touch /opt/emfa/id/ping_google
clear 

echo -----testing host: "$1":"$2" via ping-----
ping -c1 -W1 "$2" | grep 'time=' > /dev/null
#echo $?
if [ $? -eq 0 ]
then
echo OK: "$1" is up
sleep 2s
clear
else
echo -e "\e[1;31mInternet is down or google.com is not accessible so we can not download ... Aborting.\e[0m"; echo;
sleep 2s
exit 1
fi

}

## CHECK OS
function emfa_check_os () {

MACHINE_TYPE=`uname -m`

if [ ${MACHINE_TYPE} == 'x86_64' ]; then

    # 64-bit 
    printf "\n"
    printf "\e[1;42m 64-bit OS detected ...\e[0m";  printf "\n"
    printf "\n"
    
    sleep 2s
    
else 

echo -e "\e[1;31m 32-bit OS detected ... Aborting.\e[0m"; echo;

sleep 2s  
  
  exit 1
  
fi

}

## check MD5 
function emfa_check_md5() {
  # Grab md5 from file
  checksum1=$(cat $UPDATEDIR/$UPDATE.md5 | awk '{print $1}')
  # Calculate md5
  checksum2=$(md5sum $UPDATEDIR/$UPDATE | awk '{print $1}')
  if [[ "$checksum1" != "$checksum2" ]]; then
    echo "Fatal:  $UPDATE md5 checksum does not match!  Aborting..."
    abort
  fi
}

## abort install
function emfa_abort() {
  [[ -n $UPDATEDIR ]] && rm -rf $UPDATEDIR
  
  echo "Incorrect EMFABox version!"
  echo "Update to Version $VERSION FAILED.  Updates cancelled."
  echo "Please visit http://emfabox.com for more information."
  exit 1
}

## finish
function emfa_finish_update() {
  # Write the latest version number
  echo "$VERSION" > /etc/EMFA-Version

  cd /tmp
  [[ -n $UPDATEDIR ]] && rm -rf $UPDATEDIR
  
##re create dir's

if [ ! -d /opt/emfa/update ] ; then
    mkdir -p /opt/emfa/update
fi
chmod 755 /opt/emfa/update
chown apache:apache /opt/emfa/update

if [ ! -d /opt/emfa/staging ] ; then
    mkdir -p /opt/emfa/staging
fi
chmod 755 /opt/emfa/staging
chown apache:apache /opt/emfa/staging
##  
  

  echo "All done"
  echo "If your system does not return to a command prompt"
  echo "you can now safely press ctrl-c to abort logsave"
  exit 0
  exit 0
}

## disk space

function emfa_check_disk_space() {
  # Abort update if disk space is low use is >= 95%
  THRESHOLD="95"
  DISKUSE=$(df -hP -t ext4 --local | awk '{print $5}' | grep [0-9] | tr -d "%")
  for i in $DISKUSE; do
    if [[ $i -ge $THRESHOLD ]]; then

      echo "FATAL:  Update aborted.  Low disk space <=5% detected on one"
      echo "or more local ext4 filesystems!"
      echo "Free up disk space before continuing."
      echo "Please visit http://emfabox.com for assistance."

      abort
    fi
  done

}

# get current version

function emfa_get_cversion() {
  if [ -f /etc/EMFA-Version ]
    then
      CVERSION="`head -1 /etc/EMFA-Version`"
    else
      echo "ERROR: No valid version file found on this system."
      echo "ERROR: exiting now"
      exit 0
  fi

  # Check if we run an beta version
  if [[ $CVERSION =~ ^[0-9]{1}\.[0-9]{1}\.[0-9]{1}\.[0-9]{1}-beta$ ]]
    then
      echo "ERROR: You seem to be running an beta version, no upgrade possible."
      echo "ERROR: please look at http://emfabox.com for more information."
      echo "ERROR: exiting now"
      exit 0
  fi

  # Check if CVERSION is an valid Version file
  if ! [[ $CVERSION =~ ^[0-9]{1}\.[0-9]{1}\.[0-9]{1}\.[0-9]{1}$ ]]
    then
      echo "ERROR: The version file on your system does not seem to be valid."
      echo "ERROR: exiting now"
      exit 0
  fi
}

#initialize 
function emfa_initialize() {

  mkdir -p $UPDATEDIR
  cd $UPDATEDIR
}


function emfa_user_check() {
  if [ `whoami` == root ]
    then
      echo "Good you are root"
  else
    echo "Please become root to run this update"
    exit 0
  fi
}

###
function emfa_reboot() { 

 # Write the latest version number
  echo "$VERSION" > /etc/EMFA-Version

  cd /tmp
  
  rm -f emfaupgrade.sh
  
  [[ -n $UPDATEDIR ]] && rm -rf $UPDATEDIR
  
  
#re create dir's

if [ ! -d /opt/emfa/update ] ; then
    mkdir -p /opt/emfa/update
fi
chmod 755 /opt/emfa/update
chown apache:apache /opt/emfa/update

if [ ! -d /opt/emfa/staging ] ; then
    mkdir -p /opt/emfa/staging
fi
chmod 755 /opt/emfa/staging
chown apache:apache /opt/emfa/staging

  
  
#Reboot
echo "Done - Rebooting now..."
/sbin/init 6
exit 

}


### STARTUP

function emfa_startup() {
emfa_get_cversion
if [[ $CVERSION == "1.0.0.8" ]] ; then
  emfa_initialize
  emfa_update
#  emfa_finish_update
  emfa_reboot 
elif [[ $CVERSION == "1.0.0.9" ]] ; then
  emfa_initialize
  emfa_update
#  emfa_finish_update
  emfa_reboot 
else
  echo "ERROR" > /tmp/error
  emfa_abort
fi

}

####################################
# START
####################################
emfa_user_check
emfa_check_os
emfa_check_disk_space
emfa_startup
 





