DROP TABLE IF EXISTS `sys_config_global`;
CREATE TABLE `sys_config_global` (
  `variable_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_config_global
-- ----------------------------
INSERT INTO `sys_config_global` VALUES ('blacklist_relay_country_score', '1.0');
INSERT INTO `sys_config_global` VALUES ('blacklist_relay_country_codes', 'XA,XB');
INSERT INTO `sys_config_global` VALUES ('whitelist_relay_country_score', '-1.0');
INSERT INTO `sys_config_global` VALUES ('whitelist_relay_country_codes', 'XA,XB');
INSERT INTO `sys_config_global` VALUES ('blacklist_source_country_score', '1.0');
INSERT INTO `sys_config_global` VALUES ('blacklist_source_country_codes', 'XA,XB');
INSERT INTO `sys_config_global` VALUES ('whitelist_source_country_score', '-1.0');
INSERT INTO `sys_config_global` VALUES ('whitelist_source_country_codes', 'XA,XB');
