
DROP TABLE IF EXISTS `sys_blacklist_lookup_email`;
CREATE TABLE `sys_blacklist_lookup_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_country` text COLLATE utf8_unicode_ci,
  `from_address` text COLLATE utf8_unicode_ci,
  `to_address` text COLLATE utf8_unicode_ci,
  `country` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

