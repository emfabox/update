
DROP TABLE IF EXISTS `slog`;
CREATE TABLE `slog` (
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stype` text COLLATE utf8_unicode_ci,
  `hour` text COLLATE utf8_unicode_ci,
  `sid` text COLLATE utf8_unicode_ci,
  `sender` text COLLATE utf8_unicode_ci,
  `size` text COLLATE utf8_unicode_ci,
  `nrcpts` text COLLATE utf8_unicode_ci,
  `relay` text COLLATE utf8_unicode_ci,
  `rule` text COLLATE utf8_unicode_ci,
  `arg1` text COLLATE utf8_unicode_ci,
  `status` text COLLATE utf8_unicode_ci,
  `sourceid` text COLLATE utf8_unicode_ci,
  `rcpt` text COLLATE utf8_unicode_ci,
  `from_address` text COLLATE utf8_unicode_ci,
  `to_address` text COLLATE utf8_unicode_ci,
  `spam` text COLLATE utf8_unicode_ci,
  `action` text COLLATE utf8_unicode_ci,
  `file` text COLLATE utf8_unicode_ci,
  `virus` text COLLATE utf8_unicode_ci,
  `error` text COLLATE utf8_unicode_ci,
  `mech` text COLLATE utf8_unicode_ci,
  `type` text COLLATE utf8_unicode_ci,
  `score` text COLLATE utf8_unicode_ci,
  `sender_ip` text COLLATE utf8_unicode_ci,
  `cache` text COLLATE utf8_unicode_ci,
  `autolearn` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

