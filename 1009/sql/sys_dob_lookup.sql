DROP TABLE IF EXISTS `sys_dob_lookup`;
CREATE TABLE `sys_dob_lookup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arg1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `age` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relaycountry` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relay` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `to_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mx_record` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `blacklisted` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `bl_count` text COLLATE utf8_unicode_ci,
  `spf_record` text COLLATE utf8_unicode_ci,
  `score` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `to_address` (`to_address`),
  KEY `relay` (`relay`),
  KEY `arg1` (`arg1`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

