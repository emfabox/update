
DROP TABLE IF EXISTS `sys_mail_log`;
CREATE TABLE IF NOT EXISTS `sys_mail_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `log_date` datetime NOT NULL,
  `line` text NOT NULL,
  `hostname` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `process` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `process_type` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `process_id` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `message_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `requeue_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `to_address` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `orig_to` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `from_address` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `to_domain` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `from_domain` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `relay` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ip_add` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `delay` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `delays` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `dsn` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `status` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `info` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `size` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `rule` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `error_code` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `status_code` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `status_info` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `reject_by` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `reject_info` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `proto` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `helo` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `port` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `log_date` (`log_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
