
DROP TABLE IF EXISTS `sys_iprep_data_source`;
CREATE TABLE `sys_iprep_data_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `url` text COLLATE utf8_unicode_ci NOT NULL,
  `category` text COLLATE utf8_unicode_ci NOT NULL,
  `memo` text COLLATE utf8_unicode_ci NOT NULL,
  `check_frequency` int(4) DEFAULT NULL,
  `last_check` text COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_iprep_data_source
-- ----------------------------
INSERT INTO `sys_iprep_data_source` VALUES ('1', 'bambenek_c2', 'https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/bambenek_c2.ipset', 'malware', '', '30', '2015-10-18 16:00:02', 'y');
INSERT INTO `sys_iprep_data_source` VALUES ('2', 'malc0de', 'https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/malc0de.ipset', 'malware', '', '1440', '2015-10-17 16:30:03', 'y');
INSERT INTO `sys_iprep_data_source` VALUES ('3', 'palevo', 'https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/palevo.ipset', 'malware', '', '30', '2015-10-18 16:00:02', 'y');
INSERT INTO `sys_iprep_data_source` VALUES ('4', 'sslbl', 'https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/sslbl.ipset', 'malware', '', '30', '2015-10-18 16:00:02', 'y');
INSERT INTO `sys_iprep_data_source` VALUES ('5', 'cybercrime', 'https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/cybercrime.ipset', 'malware', '', '720', '2015-10-18 04:30:03', 'y');
INSERT INTO `sys_iprep_data_source` VALUES ('6', 'vxvault', 'https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/vxvault.ipset', 'malware', '', '720', '2015-10-18 04:30:03', 'y');
INSERT INTO `sys_iprep_data_source` VALUES ('7', 'stopforumspam_1d', 'https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/stopforumspam_1d.ipset', 'abuse', '', '1440', '2015-10-17 16:30:03', 'y');
INSERT INTO `sys_iprep_data_source` VALUES ('8', 'blocklist_de_mail', 'https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/blocklist_de_mail.ipset', 'attacks', 'to big', '30', '', 'n');
INSERT INTO `sys_iprep_data_source` VALUES ('9', 'botscout_1d', 'https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/botscout_1d.ipset', 'abuse', '', '30', '2015-10-18 16:00:02', 'y');
INSERT INTO `sys_iprep_data_source` VALUES ('10', 'malwaredomainlist', 'https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/malwaredomainlist.ipset', 'malware', '', '720', '2015-10-18 04:30:03', 'y');
INSERT INTO `sys_iprep_data_source` VALUES ('11', 'iw_spamlist', 'https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/iw_spamlist.ipset', 'spam', '', '60', '2015-10-18 15:30:02', 'y');
INSERT INTO `sys_iprep_data_source` VALUES ('12', 'virbl', 'https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/virbl.ipset', 'spam', '', '60', '2015-10-18 15:30:02', 'y');
INSERT INTO `sys_iprep_data_source` VALUES ('13', 'cleanmx_viruses', 'https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/cleanmx_viruses.ipset', 'spam', '', '30', '2015-10-18 16:00:02', 'y');
