-- ----------------------------
-- Table structure for `sys_rlog_config`
-- ----------------------------
DROP TABLE IF EXISTS `sys_rlog_config`;
CREATE TABLE `sys_rlog_config` (
  `variable_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_rlog_config
-- ----------------------------
INSERT INTO `sys_rlog_config` VALUES ('syslogserver', '0.0.0.0');
INSERT INTO `sys_rlog_config` VALUES ('logport', '514');
INSERT INTO `sys_rlog_config` VALUES ('logprotocol', 'udp');
INSERT INTO `sys_rlog_config` VALUES ('facility', '*');
INSERT INTO `sys_rlog_config` VALUES ('severity', '*');
INSERT INTO `sys_rlog_config` VALUES ('logtohost', 'on');
