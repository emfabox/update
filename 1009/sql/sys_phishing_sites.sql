
-- ----------------------------
-- Table structure for `sys_phishing_sites`
-- ----------------------------
DROP TABLE IF EXISTS `sys_phishing_sites`;
CREATE TABLE `sys_phishing_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `safe` enum('y','n') COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_phishing_sites
-- ----------------------------
INSERT INTO `sys_phishing_sites` VALUES ('1', 'mailscanner.info', 'mailscanner.info', 'y');
