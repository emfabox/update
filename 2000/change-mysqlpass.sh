#!/bin/bash

# creates random password (change 5 to higher digit for longer passwords)
PASS=`openssl rand -base64 5`
DBUSER="$$USERNAME$$"
DBPASS="$$PASSWORD$$"
DBNAME="$$DATABASE$$"
DBHOST="$$SERVER$$"
EMAIL="$$EMAIL$$"
MYSQL="UPDATE users SET password=MD5('$PASS') WHERE userid=10;"

mysql --host=$DBHOST --user=$DBUSER --password=$DBPASS --database=$DBNAME --execute="$MYSQL"
mail -s "Updated user password for 'office'" "$EMAIL" <<EOF

userpassword changed:

username: office
password: $PASS

EOF 