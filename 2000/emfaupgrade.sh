#!/bin/bash
########################################################################
# EMFABOX UPGRADE SCRIPT                                               #
#                                                                      #
# V1.a                                                                 #
########################################################################
# Copyright (C) 2014, 2015  http://www.cycomptec.com                   #
########################################################################
#
# Build date 30-11-2015
#
VERSION="2.0.0.0"
OLD_VERSION="1.0.0.9"
OLD_BACKUP="1.0.0.8"
logdir="/var/log/emfa"
UPDATEDIR="/tmp/EMFA-Update"
yumexclude="kernel* mysql* postfix* mailscanner* clamav* clamd* open-vm-tools*"

##########################################################################################################################
function emfa_update() {

source /etc/emfa/variables.conf

echo "Starting update to EMFABox $VERSION"

##### Backup Phase #####
#/usr/local/sbin/EMFA-Backup -backup

##### Commit Phase #####

#EMFABOX_AUTOUPDATE="no"
#sed -i '/^EMFABOX_MEMORY/a EMFABOX_AUTOUPDATE="no"' /etc/emfa/variables.conf

#update dirs
if [ ! -d /opt/emfa/update ] ; then
    mkdir -p /opt/emfa/update
fi

if [ ! -d /opt/emfa/staging ] ; then
    mkdir -p /opt/emfa/staging
fi

# update centos 
yum update -y

# php-soap for ispconfig
yum install -y php-soap

# ldap search for zimbra

yum install -y openldap-clients

#mtpolicy pre install

yum install -y perl-Cache-Memcached perl-String-CRC32 perl-Config-General perl-JSON perl-Moose perl-MooseX-Getopt  perl-MooseX-Role-Parameterized  perl-Tie-IxHash  perl-Time-Piece memcached perl-namespace-autoclean perl-MailTools




# makeself 
yum install -y makeself

#cron.d
cd /etc/cron.d

#https://bitbucket.org/emfabox/update/raw/119e3bfbf11f358eecaa8578da154c8bd74cd401/2000/etc/cron.d/emfa
/usr/bin/wget --no-check-certificate -O /etc/cron.d/emfa https://bitbucket.org/emfabox/update/raw/119e3bfbf11f358eecaa8578da154c8bd74cd401/2000/etc/cron.d/emfa
chmod 0644 /etc/cron.d/emfa


# hourly cron moved to emfa cron
cd /etc/cron.daily


#cbpolicyd_cleanup.sh
printf '#!/bin/bash\n'>/etc/cron.daily/cbpolicyd_cleanup.sh
printf '#\n'>>/etc/cron.daily/cbpolicyd_cleanup.sh
printf '\n'>>/etc/cron.daily/cbpolicyd_cleanup.sh
printf  '/usr/local/sbin/emfa/cbpadmin --cleanup >/dev/null\n'>>/etc/cron.daily/cbpolicyd_cleanup.sh

chmod 755 /etc/cron.daily/cbpolicyd_cleanup.sh



if [ -f /etc/cron.hourly/check_ldap_user ] ; then
rm -f  /etc/cron.hourly/check_ldap_user
fi

#cd /etc/cron.hourly
#cbpadmin
cd /usr/local/sbin/emfa/

#https://bitbucket.org/emfabox/update/raw/870e7796b4d45dd86d103e93f817aa4ec73e880e/2000/usr/local/sbin/emfa/cbpadmin
/usr/bin/wget --no-check-certificate -O cbpadmin https://bitbucket.org/emfabox/update/raw/870e7796b4d45dd86d103e93f817aa4ec73e880e/2000/usr/local/sbin/emfa/cbpadmin
chmod 755 cbpadmin

#cd /usr/sbin

#https://bitbucket.org/emfabox/update/raw/c9f188e91aa84bcc1e717f494fa910654edf8a53/2000/usr/local/sbin/emfa-update.sh
/usr/bin/wget --no-check-certificate -O EMFA-MS-Update https://bitbucket.org/emfabox/update/raw/c9f188e91aa84bcc1e717f494fa910654edf8a53/2000/usr/local/sbin/emfa-update.sh
chmod 755 EMFA-MS-Update


#cd /usr/local/sbin


#sudoers.d

cd /etc/sudoers.d

#EMFA-Services
#https://bitbucket.org/emfabox/update/raw/cdc79cde3efe3fe188399c7cbb07894197087d4f/1009/sudoers.d/EMFA-Services
#/usr/bin/wget --no-check-certificate -O /etc/sudoers.d/EMFA-Services https://bitbucket.org/emfabox/update/raw/cdc79cde3efe3fe188399c7cbb07894197087d4f/1009/sudoers.d/EMFA-Services


echo "apache ALL=(root) NOPASSWD: /bin/cat">>/etc/sudoers.d/EMFA-Services

visudo -c -f /etc/sudoers.d/EMFA-Services > /tmp/visudo

#ErrorDocument global ...
printf "ErrorDocument 401 /401.php\n"  > /etc/httpd/conf.d/error.conf
printf "ErrorDocument 404 /404.php\n"  >> /etc/httpd/conf.d/error.conf

########
# MailScanner

sed -i "/^Place New Headers At Top Of Message =/ c\Place New Headers At Top Of Message = yes" /etc/MailScanner/MailScanner.conf
sed -i "/^Max Spam Check Size =/ c\Max Spam Check Size = 2048k" /etc/MailScanner/MailScanner.conf
sed -i "/^Dont Sign HTML If Headers Exist =/ c\Dont Sign HTML If Headers Exist = In-Reply-To: References:" /etc/MailScanner/MailScanner.conf
sed -i "/^Notify Senders =/ c\Notify Senders = no" /etc/MailScanner/MailScanner.conf
sed -i "/^Sign Clean Messages =/ c\Sign Clean Messages = /etc/MailScanner/rules/signing.rules" /etc/MailScanner/MailScanner.conf
#Sign Clean Messages#
echo "FromOrTo: default yes" > /etc/MailScanner/rules/signing.rules


# sql
MYSQLROOTPWD=`grep MYSQLROOTPWD /etc/DB-Config | sed 's/.*://'`
now=$(date +"%m_%d_%Y")
mysqldump -u root -p"${MYSQLROOTPWD}" mailscanner > /var/www/emfa_backup/${OLD_VERSION}_$now.sql
cd  /var/www/emfa_backup/
tar -czvf  ${OLD_VERSION}_$now.sql.tar.gz ${OLD_VERSION}_$now.sql
rm -f  /var/www/emfa_backup/*.sql

cd /opt/emfa/msdbconf/

#update 

echo "UPDATE ms_config SET \`value\`='cleansigs.customize' WHERE  \`external\`='signcleanmessages'; ">/opt/emfa/msdbconf/ms_config.sql
echo "DELETE FROM ms_config WHERE options='inlinetextsig' AND \`hostname\` = '$EMFABOX_FQHOSTNAME';">>/opt/emfa/msdbconf/ms_config.sql
echo "DELETE FROM ms_config WHERE options='inlinehtmlsig' AND \`hostname\` = '$EMFABOX_FQHOSTNAME';">>/opt/emfa/msdbconf/ms_config.sql
echo "REPLACE INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', 'textsigs.customize', 'inlinetextsignature', 'inlinetextsig');">>/opt/emfa/msdbconf/ms_config.sql
echo "REPLACE INTO ms_config (\`hostname\`, \`value\`, \`external\`, \`options\`) VALUES ('$EMFABOX_FQHOSTNAME', 'htmlsigs.customize', 'inlinehtmlsignature', 'inlinehtmlsig');">>/opt/emfa/msdbconf/ms_config.sql
#echo "DELETE FROM ms_rulesets WHERE num='9999';">>/opt/emfa/msdbconf/ms_config.sql
echo "DELETE FROM ms_rulesets WHERE num='8888';">>/opt/emfa/msdbconf/ms_config.sql
echo "DELETE FROM ms_rulesets WHERE name='spammodifysubject';">>/opt/emfa/msdbconf/ms_config.sql
echo "REPLACE INTO ms_rulesets (\`name\`, \`num\`, \`rule\`) VALUES ('textsigs', '8888', 'To: default /etc/MailScanner/reports/en/inline.sig.out.txt');">>/opt/emfa/msdbconf/ms_config.sql
echo "REPLACE INTO ms_rulesets (\`name\`, \`num\`, \`rule\`) VALUES ('htmlsigs', '8888', 'To: default /etc/MailScanner/reports/en/inline.sig.out.html');">>/opt/emfa/msdbconf/ms_config.sql
echo "REPLACE INTO ms_rulesets (\`name\`, \`num\`, \`rule\`) VALUES ('cleansigs', '8888', 'To: default yes');">>/opt/emfa/msdbconf/ms_config.sql
echo "REPLACE INTO ms_rulesets (\`name\`, \`num\`, \`rule\`) VALUES ('spammodifysubject', '9999', 'To: Default no');">>/opt/emfa/msdbconf/ms_config.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/ms_config.sql


# add indexes 



#https://bitbucket.org/emfabox/update/raw/5370c66d4a7db1d015014afed8a1ce90acb0e78e/2000/sql/sys_import_tmp.sql
/usr/bin/wget --no-check-certificate -O /opt/emfa/msdbconf/sys_import_tmp.sql https://bitbucket.org/emfabox/update/raw/5370c66d4a7db1d015014afed8a1ce90acb0e78e/2000/sql/sys_import_tmp.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/sys_import_tmp.sql

#https://bitbucket.org/emfabox/update/raw/5370c66d4a7db1d015014afed8a1ce90acb0e78e/2000/sql/sys_ispconfig_conf.sql
/usr/bin/wget --no-check-certificate -O /opt/emfa/msdbconf/sys_ispconfig_conf.sql https://bitbucket.org/emfabox/update/raw/5370c66d4a7db1d015014afed8a1ce90acb0e78e/2000/sql/sys_ispconfig_conf.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/sys_ispconfig_conf.sql

# alter table 

# user table

echo "ALTER TABLE \`users\` ALTER \`domain\` DROP DEFAULT;">/opt/emfa/msdbconf/users.sql
echo "ALTER TABLE \`users\` CHANGE COLUMN \`domain\` \`domain\` VARCHAR(255) NOT NULL COLLATE \'utf8_unicode_ci\' AFTER \`num_logins\`;">>/opt/emfa/msdbconf/users.sql 
echo "ALTER TABLE \`users\` ADD COLUMN \`ldap_uid\` VARCHAR(255) NOT NULL COLLATE \'utf8_unicode_ci\' AFTER \`auth_type\`;">>/opt/emfa/msdbconf/users.sql
echo "ALTER TABLE \`users\` ADD COLUMN \`auth_name\` VARCHAR(255) NOT NULL COLLATE \'utf8_unicode_ci\' AFTER \`ldap_uid\`;">>/opt/emfa/msdbconf/users.sql
echo "ALTER TABLE \`users\` ADD COLUMN \`auth_uid\` VARCHAR(255) NOT NULL COLLATE \'utf8_unicode_ci\' AFTER \`auth_name\`;">>/opt/emfa/msdbconf/users.sql
echo "ALTER TABLE \`users\` ADD COLUMN \`auth_email\` VARCHAR(255) NOT NULL COLLATE \'utf8_unicode_ci\' AFTER \`auth_uid\`;">>/opt/emfa/msdbconf/users.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/users.sql





#echo "ALTER TABLE \`ms_config_tmp\`;" >/opt/emfa/msdbconf/ms_config_tmp.sql
#echo "ALTER \`variable_name\` DROP DEFAULT;">>/opt/emfa/msdbconf/ms_config_tmp.sql
echo "ALTER TABLE \`ms_config_tmp\` CHANGE COLUMN \`variable_name\` \`variable_name\` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci' FIRST;">/opt/emfa/msdbconf/ms_config_tmp.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/ms_config_tmp.sql


## check and repair databases  

mysqlcheck -hlocalhost -uroot -p"${MYSQLROOTPWD}" --repair --all-databases


################################################################################
#/etc/php.d/suhosin.ini

if [ -f /etc/php.d/suhosin.ini ] ; then

printf "; Enable suhosin extension module\n" > /etc/php.d/suhosin.ini
printf "extension=suhosin.so\n" >> /etc/php.d/suhosin.ini
printf "suhosin.memory_limit=2048M\n" >> /etc/php.d/suhosin.ini

fi


if [ -f /var/www/.pyzor/servers ] ; then
# add pyzor server
echo "46.101.158.108:24441" > /var/www/.pyzor/servers
echo "82.94.255.100:24441" >> /var/www/.pyzor/servers
echo "66.250.40.33:24441" >> /var/www/.pyzor/servers
fi


# cgi-script
#release-msg.cgi
#https://bitbucket.org/emfabox/update/raw/1de15600aa4c97a8e9a8bee8221f195a6e22ba0a/2000/var/www/cgi-bin/release-msg.cgi

cd /var/www/cgi-bin
/usr/bin/wget --no-check-certificate -O  /var/www/cgi-bin/release-msg.cgi https://bitbucket.org/emfabox/update/raw/1de15600aa4c97a8e9a8bee8221f195a6e22ba0a/2000/var/www/cgi-bin/release-msg.cgi
chmod 755 var/www/cgi-bin/*.cgi



# ImageCerberusPLG0 default score 
sed -i "/^score     ImageCerberusPLG0/ c\score     ImageCerberusPLG0     0.0  0.0  0.0  0.0" /etc/mail/spamassassin/ImageCerberusPLG.cf

#copy license 
if [ -f  /var/www/html/license.php ] ; then
	cp -f /var/www/html/license.php /tmp/
fi

# remove old backup
if [ -f  /var/www/emfa_backup/html-v-${OLD_BACKUP}.tar.gz ] ; then
	rm -f /var/www/emfa_backup/html-v-${OLD_BACKUP}.tar.gz
fi


#backup old version
cd /var/www/
tar -czvf html-v-${OLD_VERSION}.tar.gz html/*
mv -f html-v-${OLD_VERSION}.tar.gz  /var/www/emfa_backup/

cd /var/www/html/
rm -rf *

cd /opt/emfa/staging/
/usr/bin/wget --no-check-certificate -O /opt/emfa/staging/html.run https://s3-us-west-2.amazonaws.com/emfabox/html/html.run

chmod +x  /opt/emfa/staging/html.run 

./html.run --target /var/www/html/

rm -f  /opt/emfa/staging/html.run

# set permissions
# run script
/usr/local/sbin/set_permissions.sh

#https://s3-us-west-2.amazonaws.com/emfabox/demo/license.php
/usr/bin/wget --no-check-certificate -O /var/www/html/license.php https://s3-us-west-2.amazonaws.com/emfabox/release/AWS/demo/license.php

#move license back
if [ -f  /tmp/license.php ] ; then
	mv -f /tmp/license.php  /var/www/html/license.php
fi

#exclude update 

CHECKEXCLUDE=$(grep "^exclude" /etc/yum.conf)
if [[ -z $CHECKEXCLUDE ]]; then
echo "exclude=$yumexclude" >> /etc/yum.conf
fi

## remove old clamav database

if [ -f  /var/lib/clamav/daily.cld ] ; then
rm -f /var/lib/clamav/daily.cld
fi


}


##########################################################################################################################
### GLOBAL FUNCTIONS 
function emfa_test_host () {

if [ -f /opt/emfa/id/ping_google ] ; then

rm -rf  /opt/emfa/id/ping_google

fi;
touch /opt/emfa/id/ping_google
clear 

echo -----testing host: "$1":"$2" via ping-----
ping -c1 -W1 "$2" | grep 'time=' > /dev/null
#echo $?
if [ $? -eq 0 ]
then
echo OK: "$1" is up
sleep 2s
clear
else
echo -e "\e[1;31mInternet is down or google.com is not accessible so we can not download ... Aborting.\e[0m"; echo;
sleep 2s
exit 1
fi

}

## CHECK OS
function emfa_check_os () {

MACHINE_TYPE=`uname -m`

if [ ${MACHINE_TYPE} == 'x86_64' ]; then

    # 64-bit 
    printf "\n"
    printf "\e[1;42m 64-bit OS detected ...\e[0m";  printf "\n"
    printf "\n"
    
    sleep 2s
    
else 

echo -e "\e[1;31m 32-bit OS detected ... Aborting.\e[0m"; echo;

sleep 2s  
  
  exit 1
  
fi

}

## check MD5 
function emfa_check_md5() {
  # Grab md5 from file
  checksum1=$(cat $UPDATEDIR/$UPDATE.md5 | awk '{print $1}')
  # Calculate md5
  checksum2=$(md5sum $UPDATEDIR/$UPDATE | awk '{print $1}')
  if [[ "$checksum1" != "$checksum2" ]]; then
    echo "Fatal:  $UPDATE md5 checksum does not match!  Aborting..."
    abort
  fi
}

## abort install
function emfa_abort() {
  [[ -n $UPDATEDIR ]] && rm -rf $UPDATEDIR
  
  echo "Incorrect EMFABox version!"
  echo "Update to Version $VERSION FAILED.  Updates cancelled."
  echo "Please visit http://emfabox.com for more information."
  exit 1
}

## finish
function emfa_finish_update() {
  # Write the latest version number
  echo "$VERSION" > /etc/EMFA-Version

  cd /tmp
  [[ -n $UPDATEDIR ]] && rm -rf $UPDATEDIR
  
##re create dir's

if [ ! -d /opt/emfa/update ] ; then
    mkdir -p /opt/emfa/update
fi
chmod 755 /opt/emfa/update
chown apache:apache /opt/emfa/update

if [ ! -d /opt/emfa/staging ] ; then
    mkdir -p /opt/emfa/staging
fi
chmod 755 /opt/emfa/staging
chown apache:apache /opt/emfa/staging
##  
  

  echo "All done"
  echo "If your system does not return to a command prompt"
  echo "you can now safely press ctrl-c to abort logsave"
  exit 0
  exit 0
}

## disk space

function emfa_check_disk_space() {
  # Abort update if disk space is low use is >= 95%
  THRESHOLD="95"
  DISKUSE=$(df -hP -t ext4 --local | awk '{print $5}' | grep [0-9] | tr -d "%")
  for i in $DISKUSE; do
    if [[ $i -ge $THRESHOLD ]]; then

      echo "FATAL:  Update aborted.  Low disk space <=5% detected on one"
      echo "or more local ext4 filesystems!"
      echo "Free up disk space before continuing."
      echo "Please visit http://emfabox.com for assistance."

      abort
    fi
  done

}

# get current version

function emfa_get_cversion() {
  if [ -f /etc/EMFA-Version ]
    then
      CVERSION="`head -1 /etc/EMFA-Version`"
    else
      echo "ERROR: No valid version file found on this system."
      echo "ERROR: exiting now"
      exit 0
  fi

  # Check if we run an beta version
  if [[ $CVERSION =~ ^[0-9]{1}\.[0-9]{1}\.[0-9]{1}\.[0-9]{1}-beta$ ]]
    then
      echo "ERROR: You seem to be running an beta version, no upgrade possible."
      echo "ERROR: please look at http://emfabox.com for more information."
      echo "ERROR: exiting now"
      exit 0
  fi

  # Check if CVERSION is an valid Version file
  if ! [[ $CVERSION =~ ^[0-9]{1}\.[0-9]{1}\.[0-9]{1}\.[0-9]{1}$ ]]
    then
      echo "ERROR: The version file on your system does not seem to be valid."
      echo "ERROR: exiting now"
      exit 0
  fi
}

#initialize 
function emfa_initialize() {

  mkdir -p $UPDATEDIR
  cd $UPDATEDIR
}


function emfa_user_check() {
  if [ `whoami` == root ]
    then
      echo "Good you are root"
  else
    echo "Please become root to run this update"
    exit 0
  fi
}

###
function emfa_reboot() { 

 # Write the latest version number
  echo "$VERSION" > /etc/EMFA-Version

  cd /tmp
  
  rm -f emfaupgrade.sh
  
  [[ -n $UPDATEDIR ]] && rm -rf $UPDATEDIR
  
  
#re create dir's

if [ ! -d /opt/emfa/update ] ; then
    mkdir -p /opt/emfa/update
fi
chmod 755 /opt/emfa/update
chown apache:apache /opt/emfa/update

if [ ! -d /opt/emfa/staging ] ; then
    mkdir -p /opt/emfa/staging
fi
chmod 755 /opt/emfa/staging
chown apache:apache /opt/emfa/staging

  
  
#Reboot
echo "Done - Rebooting now..."
/sbin/init 6
exit 

}


### STARTUP

function emfa_startup() {
emfa_get_cversion
if [[ $CVERSION == "1.0.0.9" ]] ; then
  emfa_initialize
  emfa_update
#  emfa_finish_update
  emfa_reboot 
else
  echo "ERROR" > /tmp/error
  emfa_abort
fi

}

####################################
# START
####################################
emfa_user_check
emfa_check_os
emfa_check_disk_space
emfa_startup
 





