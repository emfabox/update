#!/bin/bash
#
# Sync Mysql Passwords to Slave 
#
# 01-05-2016 

export slave=$1
export rootpass=$2


if [ "$1" = "" ]; then
        echo "usage:"
        echo "emfa-cluster-mysql-sync host password"
        echo ""
        exit 0
fi

if [ "$2" = "" ]; then
        echo "usage:"
        echo "emfa-cluster-mysql-sync host password"
        echo ""
        exit 0
fi


cd /etc/emfa

cat variables.conf | sshpass -p ${rootpass} ssh root@${slave} 'cat > /etc/emfa/variables.master'
 