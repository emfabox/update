-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.1.73-log - Source distribution
-- Server OS:                    redhat-linux-gnu
-- HeidiSQL Version:             9.3.0.5036
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for cluebringer
DROP DATABASE IF EXISTS `cluebringer`;
CREATE DATABASE IF NOT EXISTS `cluebringer` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `cluebringer`;


-- Dumping structure for table cluebringer.access_control
DROP TABLE IF EXISTS `access_control`;
CREATE TABLE IF NOT EXISTS `access_control` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PolicyID` bigint(20) unsigned DEFAULT NULL,
  `Name` varchar(255) COLLATE latin1_bin NOT NULL,
  `Verdict` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `Data` text COLLATE latin1_bin,
  `Comment` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `Disabled` smallint(6) NOT NULL DEFAULT '0',
  UNIQUE KEY `ID` (`ID`),
  KEY `access_control_policyid_disabled` (`PolicyID`,`Disabled`),
  CONSTRAINT `access_control_ibfk_1` FOREIGN KEY (`PolicyID`) REFERENCES `policies` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- Data exporting was unselected.


-- Dumping structure for table cluebringer.amavis_rules
DROP TABLE IF EXISTS `amavis_rules`;
CREATE TABLE IF NOT EXISTS `amavis_rules` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PolicyID` bigint(20) unsigned DEFAULT NULL,
  `Name` varchar(255) COLLATE latin1_bin NOT NULL,
  `bypass_virus_checks` smallint(6) DEFAULT NULL,
  `bypass_virus_checks_m` smallint(6) NOT NULL DEFAULT '0',
  `bypass_banned_checks` smallint(6) DEFAULT NULL,
  `bypass_banned_checks_m` smallint(6) NOT NULL DEFAULT '0',
  `bypass_spam_checks` smallint(6) DEFAULT NULL,
  `bypass_spam_checks_m` smallint(6) NOT NULL DEFAULT '0',
  `bypass_header_checks` smallint(6) DEFAULT NULL,
  `bypass_header_checks_m` smallint(6) NOT NULL DEFAULT '0',
  `spam_tag_level` float DEFAULT NULL,
  `spam_tag_level_m` smallint(6) NOT NULL DEFAULT '0',
  `spam_tag2_level` float DEFAULT NULL,
  `spam_tag2_level_m` smallint(6) NOT NULL DEFAULT '0',
  `spam_tag3_level` float DEFAULT NULL,
  `spam_tag3_level_m` smallint(6) NOT NULL DEFAULT '0',
  `spam_kill_level` float DEFAULT NULL,
  `spam_kill_level_m` smallint(6) NOT NULL DEFAULT '0',
  `spam_dsn_cutoff_level` float DEFAULT NULL,
  `spam_dsn_cutoff_level_m` smallint(6) NOT NULL DEFAULT '0',
  `spam_quarantine_cutoff_level` float DEFAULT NULL,
  `spam_quarantine_cutoff_level_m` smallint(6) NOT NULL DEFAULT '0',
  `spam_modifies_subject` smallint(6) DEFAULT NULL,
  `spam_modifies_subject_m` smallint(6) NOT NULL DEFAULT '0',
  `spam_tag_subject` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `spam_tag_subject_m` smallint(6) NOT NULL DEFAULT '0',
  `spam_tag2_subject` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `spam_tag2_subject_m` smallint(6) NOT NULL DEFAULT '0',
  `spam_tag3_subject` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `spam_tag3_subject_m` smallint(6) NOT NULL DEFAULT '0',
  `max_message_size` bigint(20) DEFAULT NULL,
  `max_message_size_m` smallint(6) NOT NULL DEFAULT '0',
  `banned_files` text COLLATE latin1_bin,
  `banned_files_m` smallint(6) NOT NULL DEFAULT '0',
  `sender_whitelist` text COLLATE latin1_bin,
  `sender_whitelist_m` smallint(6) NOT NULL DEFAULT '0',
  `sender_blacklist` text COLLATE latin1_bin,
  `sender_blacklist_m` smallint(6) NOT NULL DEFAULT '0',
  `notify_admin_newvirus` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `notify_admin_newvirus_m` smallint(6) NOT NULL DEFAULT '0',
  `notify_admin_virus` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `notify_admin_virus_m` smallint(6) NOT NULL DEFAULT '0',
  `notify_admin_spam` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `notify_admin_spam_m` smallint(6) NOT NULL DEFAULT '0',
  `notify_admin_banned_file` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `notify_admin_banned_file_m` smallint(6) NOT NULL DEFAULT '0',
  `notify_admin_bad_header` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `notify_admin_bad_header_m` smallint(6) NOT NULL DEFAULT '0',
  `quarantine_virus` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `quarantine_virus_m` smallint(6) NOT NULL DEFAULT '0',
  `quarantine_banned_file` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `quarantine_banned_file_m` smallint(6) NOT NULL DEFAULT '0',
  `quarantine_bad_header` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `quarantine_bad_header_m` smallint(6) NOT NULL DEFAULT '0',
  `quarantine_spam` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `quarantine_spam_m` smallint(6) NOT NULL DEFAULT '0',
  `bcc_to` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `bcc_to_m` smallint(6) NOT NULL DEFAULT '0',
  `Comment` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `Disabled` smallint(6) NOT NULL DEFAULT '0',
  UNIQUE KEY `ID` (`ID`),
  KEY `PolicyID` (`PolicyID`),
  CONSTRAINT `amavis_rules_ibfk_1` FOREIGN KEY (`PolicyID`) REFERENCES `policies` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- Data exporting was unselected.


-- Dumping structure for table cluebringer.checkhelo
DROP TABLE IF EXISTS `checkhelo`;
CREATE TABLE IF NOT EXISTS `checkhelo` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PolicyID` bigint(20) unsigned DEFAULT NULL,
  `Name` varchar(255) COLLATE latin1_bin NOT NULL,
  `UseBlacklist` smallint(6) DEFAULT NULL,
  `BlacklistPeriod` bigint(20) unsigned DEFAULT NULL,
  `UseHRP` smallint(6) DEFAULT NULL,
  `HRPPeriod` bigint(20) unsigned DEFAULT NULL,
  `HRPLimit` bigint(20) unsigned DEFAULT NULL,
  `RejectInvalid` smallint(6) DEFAULT NULL,
  `RejectIP` smallint(6) DEFAULT NULL,
  `RejectUnresolvable` smallint(6) DEFAULT NULL,
  `Comment` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `Disabled` smallint(6) NOT NULL DEFAULT '0',
  UNIQUE KEY `ID` (`ID`),
  KEY `checkhelo_policyid_disabled` (`PolicyID`,`Disabled`),
  CONSTRAINT `checkhelo_ibfk_1` FOREIGN KEY (`PolicyID`) REFERENCES `policies` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- Data exporting was unselected.


-- Dumping structure for table cluebringer.checkhelo_blacklist
DROP TABLE IF EXISTS `checkhelo_blacklist`;
CREATE TABLE IF NOT EXISTS `checkhelo_blacklist` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Helo` varchar(255) COLLATE latin1_bin NOT NULL,
  `Comment` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `Disabled` smallint(6) NOT NULL DEFAULT '0',
  UNIQUE KEY `ID` (`ID`),
  UNIQUE KEY `Helo` (`Helo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- Data exporting was unselected.


-- Dumping structure for table cluebringer.checkhelo_tracking
DROP TABLE IF EXISTS `checkhelo_tracking`;
CREATE TABLE IF NOT EXISTS `checkhelo_tracking` (
  `Address` varchar(64) COLLATE latin1_bin NOT NULL,
  `Helo` varchar(255) COLLATE latin1_bin NOT NULL,
  `LastUpdate` bigint(20) unsigned NOT NULL,
  UNIQUE KEY `Address` (`Address`,`Helo`),
  KEY `checkhelo_tracking_idx1` (`LastUpdate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- Data exporting was unselected.


-- Dumping structure for table cluebringer.checkhelo_whitelist
DROP TABLE IF EXISTS `checkhelo_whitelist`;
CREATE TABLE IF NOT EXISTS `checkhelo_whitelist` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Source` varchar(512) COLLATE latin1_bin NOT NULL,
  `Comment` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `Disabled` smallint(6) NOT NULL DEFAULT '0',
  UNIQUE KEY `ID` (`ID`),
  UNIQUE KEY `Source` (`Source`),
  KEY `checkhelo_whitelist_disabled` (`Disabled`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- Data exporting was unselected.


-- Dumping structure for table cluebringer.checkspf
DROP TABLE IF EXISTS `checkspf`;
CREATE TABLE IF NOT EXISTS `checkspf` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PolicyID` bigint(20) unsigned DEFAULT NULL,
  `Name` varchar(255) COLLATE latin1_bin NOT NULL,
  `UseSPF` smallint(6) DEFAULT NULL,
  `RejectFailedSPF` smallint(6) DEFAULT NULL,
  `AddSPFHeader` smallint(6) DEFAULT NULL,
  `Comment` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `Disabled` smallint(6) NOT NULL DEFAULT '0',
  UNIQUE KEY `ID` (`ID`),
  KEY `PolicyID` (`PolicyID`),
  CONSTRAINT `checkspf_ibfk_1` FOREIGN KEY (`PolicyID`) REFERENCES `policies` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- Data exporting was unselected.


-- Dumping structure for table cluebringer.greylisting
DROP TABLE IF EXISTS `greylisting`;
CREATE TABLE IF NOT EXISTS `greylisting` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PolicyID` bigint(20) unsigned DEFAULT NULL,
  `Name` varchar(255) COLLATE latin1_bin NOT NULL,
  `UseGreylisting` smallint(6) DEFAULT NULL,
  `GreylistPeriod` bigint(20) unsigned DEFAULT NULL,
  `Track` varchar(255) COLLATE latin1_bin NOT NULL,
  `GreylistAuthValidity` bigint(20) unsigned DEFAULT NULL,
  `GreylistUnAuthValidity` bigint(20) unsigned DEFAULT NULL,
  `UseAutoWhitelist` smallint(6) DEFAULT NULL,
  `AutoWhitelistPeriod` bigint(20) unsigned DEFAULT NULL,
  `AutoWhitelistCount` bigint(20) unsigned DEFAULT NULL,
  `AutoWhitelistPercentage` bigint(20) unsigned DEFAULT NULL,
  `UseAutoBlacklist` smallint(6) DEFAULT NULL,
  `AutoBlacklistPeriod` bigint(20) unsigned DEFAULT NULL,
  `AutoBlacklistCount` bigint(20) unsigned DEFAULT NULL,
  `AutoBlacklistPercentage` bigint(20) unsigned DEFAULT NULL,
  `Comment` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `Disabled` smallint(6) NOT NULL DEFAULT '0',
  UNIQUE KEY `ID` (`ID`),
  KEY `greylisting_policyid_disabled` (`PolicyID`,`Disabled`),
  CONSTRAINT `greylisting_ibfk_1` FOREIGN KEY (`PolicyID`) REFERENCES `policies` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- Data exporting was unselected.


-- Dumping structure for table cluebringer.greylisting_autoblacklist
DROP TABLE IF EXISTS `greylisting_autoblacklist`;
CREATE TABLE IF NOT EXISTS `greylisting_autoblacklist` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `TrackKey` varchar(512) COLLATE latin1_bin NOT NULL,
  `Added` bigint(20) unsigned NOT NULL,
  `Comment` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  UNIQUE KEY `ID` (`ID`),
  UNIQUE KEY `TrackKey` (`TrackKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- Data exporting was unselected.


-- Dumping structure for table cluebringer.greylisting_autowhitelist
DROP TABLE IF EXISTS `greylisting_autowhitelist`;
CREATE TABLE IF NOT EXISTS `greylisting_autowhitelist` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `TrackKey` varchar(512) COLLATE latin1_bin NOT NULL,
  `Added` bigint(20) unsigned NOT NULL,
  `LastSeen` bigint(20) unsigned NOT NULL,
  `Comment` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  UNIQUE KEY `ID` (`ID`),
  UNIQUE KEY `TrackKey` (`TrackKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- Data exporting was unselected.


-- Dumping structure for table cluebringer.greylisting_tracking
DROP TABLE IF EXISTS `greylisting_tracking`;
CREATE TABLE IF NOT EXISTS `greylisting_tracking` (
  `TrackKey` varchar(512) COLLATE latin1_bin NOT NULL,
  `Sender` varchar(255) COLLATE latin1_bin NOT NULL,
  `Recipient` varchar(255) COLLATE latin1_bin NOT NULL,
  `FirstSeen` bigint(20) unsigned NOT NULL,
  `LastUpdate` bigint(20) unsigned NOT NULL,
  `Tries` bigint(20) unsigned NOT NULL,
  `Count` bigint(20) unsigned NOT NULL,
  UNIQUE KEY `TrackKey` (`TrackKey`,`Sender`,`Recipient`),
  KEY `greylisting_tracking_idx1` (`LastUpdate`,`Count`),
  KEY `greylisting_tracking_trackkey_firstseen` (`TrackKey`,`FirstSeen`),
  KEY `greylisting_tracking_trackkey_firstseen_count` (`TrackKey`,`FirstSeen`,`Count`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- Data exporting was unselected.


-- Dumping structure for table cluebringer.greylisting_whitelist
DROP TABLE IF EXISTS `greylisting_whitelist`;
CREATE TABLE IF NOT EXISTS `greylisting_whitelist` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Source` varchar(255) COLLATE latin1_bin NOT NULL,
  `Comment` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `Disabled` smallint(6) NOT NULL DEFAULT '0',
  UNIQUE KEY `ID` (`ID`),
  UNIQUE KEY `Source` (`Source`),
  KEY `greylisting_whitelist_disabled` (`Disabled`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- Data exporting was unselected.


-- Dumping structure for table cluebringer.policies
DROP TABLE IF EXISTS `policies`;
CREATE TABLE IF NOT EXISTS `policies` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE latin1_bin NOT NULL,
  `Priority` smallint(6) NOT NULL,
  `Description` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `Disabled` smallint(6) NOT NULL DEFAULT '0',
  UNIQUE KEY `ID` (`ID`),
  UNIQUE KEY `policies_name` (`Name`),
  KEY `policies_disabled` (`Disabled`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- Data exporting was unselected.


-- Dumping structure for table cluebringer.policy_groups
DROP TABLE IF EXISTS `policy_groups`;
CREATE TABLE IF NOT EXISTS `policy_groups` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE latin1_bin NOT NULL,
  `Disabled` smallint(6) NOT NULL DEFAULT '0',
  `Comment` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  UNIQUE KEY `ID` (`ID`),
  UNIQUE KEY `Name` (`Name`),
  UNIQUE KEY `policy_groups_name` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- Data exporting was unselected.


-- Dumping structure for table cluebringer.policy_group_members
DROP TABLE IF EXISTS `policy_group_members`;
CREATE TABLE IF NOT EXISTS `policy_group_members` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PolicyGroupID` bigint(20) unsigned DEFAULT NULL,
  `Member` varchar(255) COLLATE latin1_bin NOT NULL,
  `Disabled` smallint(6) NOT NULL DEFAULT '0',
  `Comment` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `Type` varchar(10) COLLATE latin1_bin NOT NULL DEFAULT '',
  UNIQUE KEY `ID` (`ID`),
  UNIQUE KEY `policy_group_members_policygroupid_member` (`PolicyGroupID`,`Member`),
  KEY `policy_group_members_type` (`Type`),
  KEY `policy_group_members_policygroupid_type` (`PolicyGroupID`,`Type`),
  KEY `policy_group_members_member` (`Member`),
  CONSTRAINT `policy_group_members_ibfk_1` FOREIGN KEY (`PolicyGroupID`) REFERENCES `policy_groups` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- Data exporting was unselected.


-- Dumping structure for table cluebringer.policy_members
DROP TABLE IF EXISTS `policy_members`;
CREATE TABLE IF NOT EXISTS `policy_members` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PolicyID` bigint(20) unsigned DEFAULT NULL,
  `Source` text COLLATE latin1_bin,
  `Destination` text COLLATE latin1_bin,
  `Comment` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `Disabled` smallint(6) NOT NULL DEFAULT '0',
  UNIQUE KEY `ID` (`ID`),
  KEY `PolicyID` (`PolicyID`),
  CONSTRAINT `policy_members_ibfk_1` FOREIGN KEY (`PolicyID`) REFERENCES `policies` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- Data exporting was unselected.


-- Dumping structure for table cluebringer.quotas
DROP TABLE IF EXISTS `quotas`;
CREATE TABLE IF NOT EXISTS `quotas` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PolicyID` bigint(20) unsigned DEFAULT NULL,
  `Name` varchar(255) COLLATE latin1_bin NOT NULL,
  `Track` varchar(255) COLLATE latin1_bin NOT NULL,
  `Period` bigint(20) unsigned DEFAULT NULL,
  `Verdict` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `Data` text COLLATE latin1_bin,
  `Comment` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `Disabled` smallint(6) NOT NULL DEFAULT '0',
  UNIQUE KEY `ID` (`ID`),
  KEY `quotas_policyid_disabled` (`PolicyID`,`Disabled`),
  KEY `quotas_name` (`Name`),
  CONSTRAINT `quotas_ibfk_1` FOREIGN KEY (`PolicyID`) REFERENCES `policies` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- Data exporting was unselected.


-- Dumping structure for table cluebringer.quotas_limits
DROP TABLE IF EXISTS `quotas_limits`;
CREATE TABLE IF NOT EXISTS `quotas_limits` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `QuotasID` bigint(20) unsigned DEFAULT NULL,
  `Type` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `CounterLimit` bigint(20) unsigned DEFAULT NULL,
  `Comment` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `Disabled` smallint(6) NOT NULL DEFAULT '0',
  UNIQUE KEY `ID` (`ID`),
  UNIQUE KEY `quotas_limits_quotasid_type` (`QuotasID`,`Type`),
  CONSTRAINT `quotas_limits_ibfk_1` FOREIGN KEY (`QuotasID`) REFERENCES `quotas` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- Data exporting was unselected.


-- Dumping structure for table cluebringer.quotas_tracking
DROP TABLE IF EXISTS `quotas_tracking`;
CREATE TABLE IF NOT EXISTS `quotas_tracking` (
  `QuotasLimitsID` bigint(20) unsigned DEFAULT NULL,
  `TrackKey` varchar(512) COLLATE latin1_bin DEFAULT NULL,
  `LastUpdate` bigint(20) unsigned DEFAULT NULL,
  `Counter` decimal(10,4) DEFAULT NULL,
  UNIQUE KEY `QuotasLimitsID` (`QuotasLimitsID`,`TrackKey`),
  KEY `quotas_tracking_idx1` (`LastUpdate`),
  KEY `quotas_tracking_trackkey` (`TrackKey`),
  CONSTRAINT `quotas_tracking_ibfk_1` FOREIGN KEY (`QuotasLimitsID`) REFERENCES `quotas_limits` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- Data exporting was unselected.


-- Dumping structure for table cluebringer.session_tracking
DROP TABLE IF EXISTS `session_tracking`;
CREATE TABLE IF NOT EXISTS `session_tracking` (
  `Instance` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `QueueID` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `Timestamp` bigint(20) NOT NULL,
  `ClientAddress` varchar(64) COLLATE latin1_bin DEFAULT NULL,
  `ClientName` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `ClientReverseName` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `Protocol` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `EncryptionProtocol` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `EncryptionCipher` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `EncryptionKeySize` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `SASLMethod` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `SASLSender` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `SASLUsername` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `Helo` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `Sender` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `Size` bigint(20) unsigned DEFAULT NULL,
  `RecipientData` text COLLATE latin1_bin,
  UNIQUE KEY `Instance` (`Instance`),
  KEY `session_tracking_idx1` (`QueueID`,`ClientAddress`,`Sender`),
  KEY `session_tracking_idx2` (`Timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- Data exporting was unselected.


-- Dumping database structure for cluster
DROP DATABASE IF EXISTS `cluster`;
CREATE DATABASE IF NOT EXISTS `cluster` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `cluster`;


-- Dumping structure for table cluster.settings
DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(100) DEFAULT NULL,
  `variable` varchar(100) DEFAULT NULL,
  `value` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table cluster.test
DROP TABLE IF EXISTS `test`;
CREATE TABLE IF NOT EXISTS `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `data` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping database structure for mailscanner
DROP DATABASE IF EXISTS `mailscanner`;
CREATE DATABASE IF NOT EXISTS `mailscanner` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `mailscanner`;


-- Dumping structure for table mailscanner.acct_client_address
DROP TABLE IF EXISTS `acct_client_address`;
CREATE TABLE IF NOT EXISTS `acct_client_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `count` int(10) unsigned NOT NULL,
  `count_rcpt` int(10) unsigned NOT NULL,
  `size` int(10) unsigned NOT NULL,
  `size_rcpt` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `time_key` (`key`,`time`),
  KEY `key` (`key`),
  KEY `time` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.acct_sasl_username
DROP TABLE IF EXISTS `acct_sasl_username`;
CREATE TABLE IF NOT EXISTS `acct_sasl_username` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `count` int(10) unsigned NOT NULL,
  `count_rcpt` int(10) unsigned NOT NULL,
  `size` int(10) unsigned NOT NULL,
  `size_rcpt` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `time_key` (`key`,`time`),
  KEY `key` (`key`),
  KEY `time` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.alias
DROP TABLE IF EXISTS `alias`;
CREATE TABLE IF NOT EXISTS `alias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `goto` text COLLATE utf8_unicode_ci NOT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ldap_account_suffix` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.allowed_hosts
DROP TABLE IF EXISTS `allowed_hosts`;
CREATE TABLE IF NOT EXISTS `allowed_hosts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IP` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `host_name` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `Client` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `Comments` text COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.api_servers
DROP TABLE IF EXISTS `api_servers`;
CREATE TABLE IF NOT EXISTS `api_servers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `ipaddress` varchar(255) NOT NULL,
  `password_hash` text NOT NULL,
  `api_key` varchar(32) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ipaddress` (`ipaddress`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.api_tasks
DROP TABLE IF EXISTS `api_tasks`;
CREATE TABLE IF NOT EXISTS `api_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task` text NOT NULL,
  `sender_name` text NOT NULL,
  `sender_ip` text NOT NULL,
  `api_key` varchar(32) NOT NULL,
  `var1` text NOT NULL,
  `var2` text NOT NULL,
  `var3` text NOT NULL,
  `var4` text NOT NULL,
  `var5` text NOT NULL,
  `var6` text NOT NULL,
  `var7` text NOT NULL,
  `var8` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.api_users
DROP TABLE IF EXISTS `api_users`;
CREATE TABLE IF NOT EXISTS `api_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` text NOT NULL,
  `api_key` varchar(32) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.audit_log
DROP TABLE IF EXISTS `audit_log`;
CREATE TABLE IF NOT EXISTS `audit_log` (
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ip_address` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `action` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.blacklist
DROP TABLE IF EXISTS `blacklist`;
CREATE TABLE IF NOT EXISTS `blacklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `to_address` text COLLATE utf8_unicode_ci,
  `to_domain` text COLLATE utf8_unicode_ci,
  `from_address` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `blacklist_uniq` (`to_address`(100),`from_address`(100))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.captcha
DROP TABLE IF EXISTS `captcha`;
CREATE TABLE IF NOT EXISTS `captcha` (
  `captcha_id` int(11) NOT NULL AUTO_INCREMENT,
  `captcha_phpsessid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `captcha_time` int(11) NOT NULL,
  `captcha_captcha` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`captcha_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.dkim
DROP TABLE IF EXISTS `dkim`;
CREATE TABLE IF NOT EXISTS `dkim` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `selector` varchar(63) COLLATE utf8_unicode_ci NOT NULL,
  `private_key` text COLLATE utf8_unicode_ci,
  `public_key` text COLLATE utf8_unicode_ci,
  `dns_text` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.dkim_config
DROP TABLE IF EXISTS `dkim_config`;
CREATE TABLE IF NOT EXISTS `dkim_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `domain_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `selector` varchar(63) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  `private_key` text COLLATE utf8_unicode_ci,
  `public_key` text COLLATE utf8_unicode_ci,
  `SigningTable` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KeyTable` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.dkim_trustedhosts
DROP TABLE IF EXISTS `dkim_trustedhosts`;
CREATE TABLE IF NOT EXISTS `dkim_trustedhosts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `network` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.dnsbl_domains
DROP TABLE IF EXISTS `dnsbl_domains`;
CREATE TABLE IF NOT EXISTS `dnsbl_domains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.dobrblconfig
DROP TABLE IF EXISTS `dobrblconfig`;
CREATE TABLE IF NOT EXISTS `dobrblconfig` (
  `setting` text COLLATE utf8_unicode_ci,
  `value` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.domain
DROP TABLE IF EXISTS `domain`;
CREATE TABLE IF NOT EXISTS `domain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `aliases` int(10) NOT NULL DEFAULT '0',
  `mailboxes` int(10) NOT NULL DEFAULT '0',
  `maxquota` bigint(20) NOT NULL DEFAULT '0',
  `quota` bigint(20) NOT NULL DEFAULT '0',
  `transport` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'dovecot',
  `desthost` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `backupmx` tinyint(1) NOT NULL DEFAULT '0',
  `avscan` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `opscan` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `antispoofing` varchar(55) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'REJECT',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `domain` (`domain`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.domain_admins
DROP TABLE IF EXISTS `domain_admins`;
CREATE TABLE IF NOT EXISTS `domain_admins` (
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.domain_filters
DROP TABLE IF EXISTS `domain_filters`;
CREATE TABLE IF NOT EXISTS `domain_filters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `domain` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `filter` mediumtext COLLATE utf8_unicode_ci,
  `active` enum('N','Y') COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `domain_filters_domain_idx` (`domain`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.filename
DROP TABLE IF EXISTS `filename`;
CREATE TABLE IF NOT EXISTS `filename` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `rule` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ext` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.filetype
DROP TABLE IF EXISTS `filetype`;
CREATE TABLE IF NOT EXISTS `filetype` (
  `mid` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `rule` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ext` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.geoip_country
DROP TABLE IF EXISTS `geoip_country`;
CREATE TABLE IF NOT EXISTS `geoip_country` (
  `begin_ip` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_ip` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `begin_num` bigint(20) DEFAULT NULL,
  `end_num` bigint(20) DEFAULT NULL,
  `iso_country_code` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` text COLLATE utf8_unicode_ci,
  KEY `geoip_country_begin` (`begin_num`),
  KEY `geoip_country_end` (`end_num`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.imp_mailuser
DROP TABLE IF EXISTS `imp_mailuser`;
CREATE TABLE IF NOT EXISTS `imp_mailuser` (
  `imp_userid` int(11) NOT NULL AUTO_INCREMENT,
  `sys_userid` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `sys_groupid` int(11) unsigned NOT NULL DEFAULT '0',
  `sys_perm_user` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sys_perm_group` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sys_perm_other` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ad_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ad_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ad_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ad_username` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ad_functions` text COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`imp_userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.inq
DROP TABLE IF EXISTS `inq`;
CREATE TABLE IF NOT EXISTS `inq` (
  `id` mediumtext COLLATE utf8_unicode_ci,
  `cdate` date DEFAULT NULL,
  `ctime` time DEFAULT NULL,
  `from_address` mediumtext COLLATE utf8_unicode_ci,
  `to_address` mediumtext COLLATE utf8_unicode_ci,
  `subject` mediumtext COLLATE utf8_unicode_ci,
  `message` mediumtext COLLATE utf8_unicode_ci,
  `size` mediumtext COLLATE utf8_unicode_ci,
  `priority` mediumtext COLLATE utf8_unicode_ci,
  `attempts` mediumtext COLLATE utf8_unicode_ci,
  `lastattempt` mediumtext COLLATE utf8_unicode_ci,
  `hostname` mediumtext COLLATE utf8_unicode_ci,
  KEY `inq_hostname` (`hostname`(50))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.loginAttempts
DROP TABLE IF EXISTS `loginAttempts`;
CREATE TABLE IF NOT EXISTS `loginAttempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip` varchar(128) NOT NULL DEFAULT '',
  `count` int(2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ip` (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.lusers
DROP TABLE IF EXISTS `lusers`;
CREATE TABLE IF NOT EXISTS `lusers` (
  `lusername` text COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`lusername`(255))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.mailbox
DROP TABLE IF EXISTS `mailbox`;
CREATE TABLE IF NOT EXISTS `mailbox` (
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `storagebasedirectory` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `maildir` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `quota` bigint(20) NOT NULL DEFAULT '0',
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `department` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `rank` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'normal',
  `employeeid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enablesmtp` tinyint(1) NOT NULL DEFAULT '1',
  `enablepop3` tinyint(1) NOT NULL DEFAULT '1',
  `enableimap` tinyint(1) NOT NULL DEFAULT '1',
  `enabledeliver` tinyint(1) NOT NULL DEFAULT '1',
  `enablemanagesieve` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.maillog
DROP TABLE IF EXISTS `maillog`;
CREATE TABLE IF NOT EXISTS `maillog` (
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id` mediumtext COLLATE utf8_unicode_ci,
  `size` bigint(20) DEFAULT '0',
  `from_address` mediumtext COLLATE utf8_unicode_ci,
  `from_domain` mediumtext COLLATE utf8_unicode_ci,
  `to_address` mediumtext COLLATE utf8_unicode_ci,
  `to_domain` mediumtext COLLATE utf8_unicode_ci,
  `subject` mediumtext COLLATE utf8_unicode_ci,
  `clientip` mediumtext COLLATE utf8_unicode_ci,
  `archive` mediumtext COLLATE utf8_unicode_ci,
  `isspam` tinyint(1) DEFAULT '0',
  `ishighspam` tinyint(1) DEFAULT '0',
  `issaspam` tinyint(1) DEFAULT '0',
  `isrblspam` tinyint(1) DEFAULT '0',
  `isfp` tinyint(1) DEFAULT '0',
  `isfn` tinyint(1) DEFAULT '0',
  `spamwhitelisted` tinyint(1) DEFAULT '0',
  `spamblacklisted` tinyint(1) DEFAULT '0',
  `sascore` decimal(7,2) DEFAULT '0.00',
  `spamreport` mediumtext COLLATE utf8_unicode_ci,
  `virusinfected` tinyint(1) DEFAULT '0',
  `nameinfected` tinyint(1) DEFAULT '0',
  `otherinfected` tinyint(1) DEFAULT '0',
  `report` mediumtext COLLATE utf8_unicode_ci,
  `ismcp` tinyint(1) DEFAULT '0',
  `ishighmcp` tinyint(1) DEFAULT '0',
  `issamcp` tinyint(1) DEFAULT '0',
  `mcpwhitelisted` tinyint(1) DEFAULT '0',
  `mcpblacklisted` tinyint(1) DEFAULT '0',
  `mcpsascore` decimal(7,2) DEFAULT '0.00',
  `mcpreport` mediumtext COLLATE utf8_unicode_ci,
  `hostname` mediumtext COLLATE utf8_unicode_ci,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `headers` mediumtext COLLATE utf8_unicode_ci,
  `quarantined` tinyint(1) DEFAULT '0',
  KEY `maillog_datetime_idx` (`date`,`time`),
  KEY `maillog_id_idx` (`id`(20)),
  KEY `maillog_clientip_idx` (`clientip`(20)),
  KEY `maillog_from_idx` (`from_address`(200)),
  KEY `maillog_to_idx` (`to_address`(200)),
  KEY `maillog_host` (`hostname`(30)),
  KEY `from_domain_idx` (`from_domain`(50)),
  KEY `to_domain_idx` (`to_domain`(50)),
  KEY `maillog_quarantined` (`quarantined`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.mail_smtp_spam_blacklists
DROP TABLE IF EXISTS `mail_smtp_spam_blacklists`;
CREATE TABLE IF NOT EXISTS `mail_smtp_spam_blacklists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.mcp_rules
DROP TABLE IF EXISTS `mcp_rules`;
CREATE TABLE IF NOT EXISTS `mcp_rules` (
  `rule` char(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `rule_desc` char(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`rule`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.ms_archive_fname_rules
DROP TABLE IF EXISTS `ms_archive_fname_rules`;
CREATE TABLE IF NOT EXISTS `ms_archive_fname_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `build` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ftype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fext` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fdesc` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fdefault` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.ms_archive_ftype_rules
DROP TABLE IF EXISTS `ms_archive_ftype_rules`;
CREATE TABLE IF NOT EXISTS `ms_archive_ftype_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `build` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ftype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fext` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fdesc` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fdefault` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.ms_config
DROP TABLE IF EXISTS `ms_config`;
CREATE TABLE IF NOT EXISTS `ms_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `external` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `options` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.ms_config_tmp
DROP TABLE IF EXISTS `ms_config_tmp`;
CREATE TABLE IF NOT EXISTS `ms_config_tmp` (
  `variable_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.ms_filename_rules
DROP TABLE IF EXISTS `ms_filename_rules`;
CREATE TABLE IF NOT EXISTS `ms_filename_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `build` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ftype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fext` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fdesc` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fdefault` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.ms_filetype_rules
DROP TABLE IF EXISTS `ms_filetype_rules`;
CREATE TABLE IF NOT EXISTS `ms_filetype_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `build` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ftype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fext` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fdesc` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fdefault` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.ms_rulesets
DROP TABLE IF EXISTS `ms_rulesets`;
CREATE TABLE IF NOT EXISTS `ms_rulesets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `num` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `rule` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.mtalog
DROP TABLE IF EXISTS `mtalog`;
CREATE TABLE IF NOT EXISTS `mtalog` (
  `timestamp` datetime DEFAULT NULL,
  `host` mediumtext COLLATE utf8_unicode_ci,
  `type` mediumtext COLLATE utf8_unicode_ci,
  `msg_id` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relay` mediumtext COLLATE utf8_unicode_ci,
  `dsn` mediumtext COLLATE utf8_unicode_ci,
  `status` mediumtext COLLATE utf8_unicode_ci,
  `delay` time DEFAULT NULL,
  UNIQUE KEY `mtalog_uniq` (`timestamp`,`host`(10),`type`(10),`msg_id`,`relay`(20)),
  KEY `mtalog_timestamp` (`timestamp`),
  KEY `mtalog_type` (`type`(10)),
  KEY `msg_id` (`msg_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.mtalog_ids
DROP TABLE IF EXISTS `mtalog_ids`;
CREATE TABLE IF NOT EXISTS `mtalog_ids` (
  `smtpd_id` varchar(20) CHARACTER SET ascii DEFAULT NULL,
  `smtp_id` varchar(20) CHARACTER SET ascii DEFAULT NULL,
  UNIQUE KEY `mtalog_ids_idx` (`smtpd_id`,`smtp_id`),
  KEY `smtpd_id` (`smtpd_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.mwrblblock
DROP TABLE IF EXISTS `mwrblblock`;
CREATE TABLE IF NOT EXISTS `mwrblblock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientip` text COLLATE utf8_unicode_ci NOT NULL,
  `hostname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expires` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `errormsg` text COLLATE utf8_unicode_ci NOT NULL,
  `created_record` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `dob` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.mwrblclients
DROP TABLE IF EXISTS `mwrblclients`;
CREATE TABLE IF NOT EXISTS `mwrblclients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientip` text COLLATE utf8_unicode_ci NOT NULL,
  `hostname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hour` smallint(6) NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `ham` int(11) NOT NULL DEFAULT '0',
  `spam` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.mwrblconfig
DROP TABLE IF EXISTS `mwrblconfig`;
CREATE TABLE IF NOT EXISTS `mwrblconfig` (
  `setting` text COLLATE utf8_unicode_ci,
  `value` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.mwrblwlist
DROP TABLE IF EXISTS `mwrblwlist`;
CREATE TABLE IF NOT EXISTS `mwrblwlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `clientip` text COLLATE utf8_unicode_ci NOT NULL,
  `hostname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.outq
DROP TABLE IF EXISTS `outq`;
CREATE TABLE IF NOT EXISTS `outq` (
  `id` mediumtext COLLATE utf8_unicode_ci,
  `cdate` date DEFAULT NULL,
  `ctime` time DEFAULT NULL,
  `from_address` mediumtext COLLATE utf8_unicode_ci,
  `to_address` mediumtext COLLATE utf8_unicode_ci,
  `subject` mediumtext COLLATE utf8_unicode_ci,
  `message` mediumtext COLLATE utf8_unicode_ci,
  `size` mediumtext COLLATE utf8_unicode_ci,
  `priority` mediumtext COLLATE utf8_unicode_ci,
  `attempts` mediumtext COLLATE utf8_unicode_ci,
  `lastattempt` mediumtext COLLATE utf8_unicode_ci,
  `hostname` mediumtext COLLATE utf8_unicode_ci,
  KEY `outq_hostname` (`hostname`(50))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.recipient_bcc_domain
DROP TABLE IF EXISTS `recipient_bcc_domain`;
CREATE TABLE IF NOT EXISTS `recipient_bcc_domain` (
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bcc_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`domain`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.recipient_bcc_user
DROP TABLE IF EXISTS `recipient_bcc_user`;
CREATE TABLE IF NOT EXISTS `recipient_bcc_user` (
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bcc_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.saved_filters
DROP TABLE IF EXISTS `saved_filters`;
CREATE TABLE IF NOT EXISTS `saved_filters` (
  `name` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `col` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `operator` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `username` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_filters` (`name`(20),`col`(20),`operator`(20),`value`(20),`username`(20))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sa_rules
DROP TABLE IF EXISTS `sa_rules`;
CREATE TABLE IF NOT EXISTS `sa_rules` (
  `rule` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `rule_desc` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`rule`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sa_users
DROP TABLE IF EXISTS `sa_users`;
CREATE TABLE IF NOT EXISTS `sa_users` (
  `id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(60) COLLATE utf8_unicode_ci DEFAULT '',
  `fullname` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` enum('A','D','U','R','H') COLLATE utf8_unicode_ci DEFAULT NULL,
  `quarantine_report` tinyint(1) DEFAULT '0',
  `spamscore` tinyint(4) DEFAULT '0',
  `highspamscore` tinyint(4) DEFAULT '0',
  `noscan` tinyint(1) DEFAULT '0',
  `quarantine_rcpt` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule_id` int(12) DEFAULT '9999',
  `clean_store` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `clean_deliver` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `clean_strip_html` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `clean_header` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `clean_header_txt` varchar(40) COLLATE utf8_unicode_ci DEFAULT 'X-Spam-Status: No',
  `spam_store` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `spam_deliver` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `spam_modify` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `spam_modify_txt` varchar(40) COLLATE utf8_unicode_ci DEFAULT '{Spam?}',
  `spam_notify` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `spam_delete` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `spam_bounce` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `spam_forward` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `spam_forward_txt` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spam_strip_html` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `spam_attachement` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `spam_header` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `spam_header_txt` varchar(40) COLLATE utf8_unicode_ci DEFAULT 'X-Spam-Status: Yes',
  `high_spam_store` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `high_spam_deliver` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `high_spam_notify` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `high_spam_delete` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `high_spam_bounce` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `high_spam_forward` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `high_spam_forward_txt` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `high_spam_strip_html` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'n',
  `high_spam_attachement` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `high_spam_header` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `high_spam_header_txt` varchar(40) COLLATE utf8_unicode_ci DEFAULT 'X-Spam-Status: Yes',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sender_bcc_domain
DROP TABLE IF EXISTS `sender_bcc_domain`;
CREATE TABLE IF NOT EXISTS `sender_bcc_domain` (
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bcc_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`domain`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sender_bcc_user
DROP TABLE IF EXISTS `sender_bcc_user`;
CREATE TABLE IF NOT EXISTS `sender_bcc_user` (
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bcc_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.slog
DROP TABLE IF EXISTS `slog`;
CREATE TABLE IF NOT EXISTS `slog` (
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stype` text COLLATE utf8_unicode_ci,
  `hour` text COLLATE utf8_unicode_ci,
  `sid` text COLLATE utf8_unicode_ci,
  `sender` text COLLATE utf8_unicode_ci,
  `size` text COLLATE utf8_unicode_ci,
  `nrcpts` text COLLATE utf8_unicode_ci,
  `relay` text COLLATE utf8_unicode_ci,
  `rule` text COLLATE utf8_unicode_ci,
  `arg1` text COLLATE utf8_unicode_ci,
  `status` text COLLATE utf8_unicode_ci,
  `sourceid` text COLLATE utf8_unicode_ci,
  `rcpt` text COLLATE utf8_unicode_ci,
  `from_address` text COLLATE utf8_unicode_ci,
  `to_address` text COLLATE utf8_unicode_ci,
  `spam` text COLLATE utf8_unicode_ci,
  `action` text COLLATE utf8_unicode_ci,
  `file` text COLLATE utf8_unicode_ci,
  `virus` text COLLATE utf8_unicode_ci,
  `error` text COLLATE utf8_unicode_ci,
  `mech` text COLLATE utf8_unicode_ci,
  `type` text COLLATE utf8_unicode_ci,
  `score` text COLLATE utf8_unicode_ci,
  `sender_ip` text COLLATE utf8_unicode_ci,
  `cache` text COLLATE utf8_unicode_ci,
  `autolearn` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.slogconfig
DROP TABLE IF EXISTS `slogconfig`;
CREATE TABLE IF NOT EXISTS `slogconfig` (
  `setting` text COLLATE utf8_unicode_ci,
  `value` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.spamscores
DROP TABLE IF EXISTS `spamscores`;
CREATE TABLE IF NOT EXISTS `spamscores` (
  `user` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lowspamscore` decimal(10,0) NOT NULL DEFAULT '0',
  `highspamscore` decimal(10,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_addon_blacklist
DROP TABLE IF EXISTS `sys_addon_blacklist`;
CREATE TABLE IF NOT EXISTS `sys_addon_blacklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `relay_ip` text COLLATE utf8_unicode_ci,
  `blocklist` text COLLATE utf8_unicode_ci,
  `info` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_addon_config
DROP TABLE IF EXISTS `sys_addon_config`;
CREATE TABLE IF NOT EXISTS `sys_addon_config` (
  `setting` text COLLATE utf8_unicode_ci,
  `value` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_addon_lookup
DROP TABLE IF EXISTS `sys_addon_lookup`;
CREATE TABLE IF NOT EXISTS `sys_addon_lookup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `relay` text COLLATE utf8_unicode_ci,
  `relay_ip` text COLLATE utf8_unicode_ci,
  `country` text COLLATE utf8_unicode_ci,
  `from_address` text COLLATE utf8_unicode_ci,
  `tld` text COLLATE utf8_unicode_ci,
  `created` text COLLATE utf8_unicode_ci,
  `action` text COLLATE utf8_unicode_ci,
  `to_address` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_api_auth_token
DROP TABLE IF EXISTS `sys_api_auth_token`;
CREATE TABLE IF NOT EXISTS `sys_api_auth_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `domain` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'n',
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_UNIQUE` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_api_permit
DROP TABLE IF EXISTS `sys_api_permit`;
CREATE TABLE IF NOT EXISTS `sys_api_permit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_blacklist_lookup
DROP TABLE IF EXISTS `sys_blacklist_lookup`;
CREATE TABLE IF NOT EXISTS `sys_blacklist_lookup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_domain` text COLLATE utf8_unicode_ci,
  `to_domain` text COLLATE utf8_unicode_ci,
  `active` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_blacklist_lookup_country
DROP TABLE IF EXISTS `sys_blacklist_lookup_country`;
CREATE TABLE IF NOT EXISTS `sys_blacklist_lookup_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_country` text COLLATE utf8_unicode_ci,
  `from_domain` text COLLATE utf8_unicode_ci,
  `to_domain` text COLLATE utf8_unicode_ci,
  `country` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_blacklist_lookup_email
DROP TABLE IF EXISTS `sys_blacklist_lookup_email`;
CREATE TABLE IF NOT EXISTS `sys_blacklist_lookup_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_country` text COLLATE utf8_unicode_ci,
  `from_address` text COLLATE utf8_unicode_ci,
  `to_address` text COLLATE utf8_unicode_ci,
  `country` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_box_firewall
DROP TABLE IF EXISTS `sys_box_firewall`;
CREATE TABLE IF NOT EXISTS `sys_box_firewall` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `num` int(11) unsigned NOT NULL DEFAULT '0',
  `traffic` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `pkts` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `bytes` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `target` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `prot` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `opt` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `tin` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `tout` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `source` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `destination` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `dest_entry` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dest_port` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_box_fw_ports
DROP TABLE IF EXISTS `sys_box_fw_ports`;
CREATE TABLE IF NOT EXISTS `sys_box_fw_ports` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prot` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `var` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `var2` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_box_fw_rule
DROP TABLE IF EXISTS `sys_box_fw_rule`;
CREATE TABLE IF NOT EXISTS `sys_box_fw_rule` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `traffic` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `ruleset` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_config
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE IF NOT EXISTS `sys_config` (
  `variable_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_config_global
DROP TABLE IF EXISTS `sys_config_global`;
CREATE TABLE IF NOT EXISTS `sys_config_global` (
  `variable_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_config_policyd_spf
DROP TABLE IF EXISTS `sys_config_policyd_spf`;
CREATE TABLE IF NOT EXISTS `sys_config_policyd_spf` (
  `variable_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_config_postfix
DROP TABLE IF EXISTS `sys_config_postfix`;
CREATE TABLE IF NOT EXISTS `sys_config_postfix` (
  `variable_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_config_postfix_default
DROP TABLE IF EXISTS `sys_config_postfix_default`;
CREATE TABLE IF NOT EXISTS `sys_config_postfix_default` (
  `variable_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_config_postfix_out
DROP TABLE IF EXISTS `sys_config_postfix_out`;
CREATE TABLE IF NOT EXISTS `sys_config_postfix_out` (
  `variable_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_config_postfix_select
DROP TABLE IF EXISTS `sys_config_postfix_select`;
CREATE TABLE IF NOT EXISTS `sys_config_postfix_select` (
  `variable_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_config_signature
DROP TABLE IF EXISTS `sys_config_signature`;
CREATE TABLE IF NOT EXISTS `sys_config_signature` (
  `variable_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_config_signature_rules
DROP TABLE IF EXISTS `sys_config_signature_rules`;
CREATE TABLE IF NOT EXISTS `sys_config_signature_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `num` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `match` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ruleset` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_config_sqlgrey
DROP TABLE IF EXISTS `sys_config_sqlgrey`;
CREATE TABLE IF NOT EXISTS `sys_config_sqlgrey` (
  `variable_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_datafeed
DROP TABLE IF EXISTS `sys_datafeed`;
CREATE TABLE IF NOT EXISTS `sys_datafeed` (
  `domain` text COLLATE utf8_unicode_ci,
  `ipaddress` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dateadded` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reportedby` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `attacknotes` text COLLATE utf8_unicode_ci,
  `list` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ipaddress`),
  KEY `dateadded` (`dateadded`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_dns_forwarders
DROP TABLE IF EXISTS `sys_dns_forwarders`;
CREATE TABLE IF NOT EXISTS `sys_dns_forwarders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` text COLLATE utf8_unicode_ci NOT NULL,
  `ip1` text COLLATE utf8_unicode_ci NOT NULL,
  `ip2` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_dob_lookup
DROP TABLE IF EXISTS `sys_dob_lookup`;
CREATE TABLE IF NOT EXISTS `sys_dob_lookup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hostname` text COLLATE utf8_unicode_ci,
  `stype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arg1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `age` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relaycountry` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relay` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `to_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mx_record` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `blacklisted` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `bl_count` text COLLATE utf8_unicode_ci,
  `spf_record` text COLLATE utf8_unicode_ci,
  `score` text COLLATE utf8_unicode_ci,
  `lscore` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `to_address` (`to_address`),
  KEY `relay` (`relay`),
  KEY `arg1` (`arg1`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_emfa_usage
DROP TABLE IF EXISTS `sys_emfa_usage`;
CREATE TABLE IF NOT EXISTS `sys_emfa_usage` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` text COLLATE utf8_unicode_ci,
  `domain` text COLLATE utf8_unicode_ci,
  `avira_opt` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_seen` datetime DEFAULT NULL,
  `alias` varchar(120) COLLATE utf8_unicode_ci DEFAULT 'NO',
  `uuid` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'NO',
  PRIMARY KEY (`id`),
  KEY `domain` (`domain`(50)),
  KEY `email` (`email`(200))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_fail2ban
DROP TABLE IF EXISTS `sys_fail2ban`;
CREATE TABLE IF NOT EXISTS `sys_fail2ban` (
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `service` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ipaddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_filename_rules
DROP TABLE IF EXISTS `sys_filename_rules`;
CREATE TABLE IF NOT EXISTS `sys_filename_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_ext` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `allow` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_filetype_rules
DROP TABLE IF EXISTS `sys_filetype_rules`;
CREATE TABLE IF NOT EXISTS `sys_filetype_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_type` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `allow` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_filetype_select
DROP TABLE IF EXISTS `sys_filetype_select`;
CREATE TABLE IF NOT EXISTS `sys_filetype_select` (
  `variable_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'y',
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_gateway_usage
DROP TABLE IF EXISTS `sys_gateway_usage`;
CREATE TABLE IF NOT EXISTS `sys_gateway_usage` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` text COLLATE utf8_unicode_ci,
  `domain` text COLLATE utf8_unicode_ci,
  `virus_opt` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_seen` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `domain` (`domain`(50)),
  KEY `email` (`email`(200))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_gateway_warning
DROP TABLE IF EXISTS `sys_gateway_warning`;
CREATE TABLE IF NOT EXISTS `sys_gateway_warning` (
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reason` text COLLATE utf8_unicode_ci,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  KEY `timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_hosts_file
DROP TABLE IF EXISTS `sys_hosts_file`;
CREATE TABLE IF NOT EXISTS `sys_hosts_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aliases` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_import_tmp
DROP TABLE IF EXISTS `sys_import_tmp`;
CREATE TABLE IF NOT EXISTS `sys_import_tmp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `login` varchar(60) COLLATE utf8_unicode_ci DEFAULT '',
  `password` varchar(60) COLLATE utf8_unicode_ci DEFAULT '',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `access` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `disablesmtp` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_iprep_data
DROP TABLE IF EXISTS `sys_iprep_data`;
CREATE TABLE IF NOT EXISTS `sys_iprep_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostip` text COLLATE utf8_unicode_ci NOT NULL,
  `hostname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` text COLLATE utf8_unicode_ci NOT NULL,
  `source` text COLLATE utf8_unicode_ci NOT NULL,
  `created_record` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type` text COLLATE utf8_unicode_ci NOT NULL,
  `expires` datetime DEFAULT NULL,
  `score` text COLLATE utf8_unicode_ci,
  `rep` text COLLATE utf8_unicode_ci,
  `b_or_w` enum('w','b') COLLATE utf8_unicode_ci DEFAULT 'b',
  `memo` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_iprep_data_source
DROP TABLE IF EXISTS `sys_iprep_data_source`;
CREATE TABLE IF NOT EXISTS `sys_iprep_data_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `url` text COLLATE utf8_unicode_ci NOT NULL,
  `category` text COLLATE utf8_unicode_ci NOT NULL,
  `memo` text COLLATE utf8_unicode_ci NOT NULL,
  `check_frequency` int(4) DEFAULT NULL,
  `last_check` text COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_iprep_domain_data
DROP TABLE IF EXISTS `sys_iprep_domain_data`;
CREATE TABLE IF NOT EXISTS `sys_iprep_domain_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostip` text COLLATE utf8_unicode_ci NOT NULL,
  `hostname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` text COLLATE utf8_unicode_ci NOT NULL,
  `source` text COLLATE utf8_unicode_ci NOT NULL,
  `created_record` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type` text COLLATE utf8_unicode_ci NOT NULL,
  `expires` datetime NOT NULL,
  `memo` text COLLATE utf8_unicode_ci,
  `rep` text COLLATE utf8_unicode_ci,
  `b_or_w` enum('w','b') COLLATE utf8_unicode_ci DEFAULT 'b',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_iprep_wl_data
DROP TABLE IF EXISTS `sys_iprep_wl_data`;
CREATE TABLE IF NOT EXISTS `sys_iprep_wl_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostip` text COLLATE utf8_unicode_ci NOT NULL,
  `hostname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` text COLLATE utf8_unicode_ci NOT NULL,
  `source` text COLLATE utf8_unicode_ci NOT NULL,
  `created_record` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type` text COLLATE utf8_unicode_ci NOT NULL,
  `memo` text COLLATE utf8_unicode_ci,
  `rep` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_ispconfig_conf
DROP TABLE IF EXISTS `sys_ispconfig_conf`;
CREATE TABLE IF NOT EXISTS `sys_ispconfig_conf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ispconfig_domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `soap_location` text COLLATE utf8_unicode_ci NOT NULL,
  `soap_uri` text COLLATE utf8_unicode_ci,
  `soap_user` text COLLATE utf8_unicode_ci NOT NULL,
  `soap_pass` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purge_user` enum('y','n') COLLATE utf8_unicode_ci DEFAULT 'n',
  `cronjob` enum('y','n') COLLATE utf8_unicode_ci DEFAULT 'n',
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_kernel_routes
DROP TABLE IF EXISTS `sys_kernel_routes`;
CREATE TABLE IF NOT EXISTS `sys_kernel_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gateway` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `genmask` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metric` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ref` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `use` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iface` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_ldap_conf
DROP TABLE IF EXISTS `sys_ldap_conf`;
CREATE TABLE IF NOT EXISTS `sys_ldap_conf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ldap_email_domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `ldap_dc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_dc_ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_dc_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_dc_2_ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_dn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_base_dn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_email_base_dn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_bind_dn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_bind_pass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_account_suffix` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_filter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_auth_search` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_attributes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_authrealm` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_ad_domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_exchange_ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_port` int(11) NOT NULL DEFAULT '389',
  `ldap_proxyuser` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_proxypass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_proxy_addresses` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_active` tinyint(1) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `exchange_lookup` int(11) NOT NULL DEFAULT '0',
  `exchange_sync` int(11) NOT NULL DEFAULT '0',
  `sync_id` int(2) DEFAULT NULL,
  `sync_time` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `auth_domain` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `cronjob` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `purge_user` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'n',
  `ldaps` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_ldap_cron_defaults
DROP TABLE IF EXISTS `sys_ldap_cron_defaults`;
CREATE TABLE IF NOT EXISTS `sys_ldap_cron_defaults` (
  `id` int(2) DEFAULT NULL,
  `value` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `option` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'n'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_ldap_imp_dyn
DROP TABLE IF EXISTS `sys_ldap_imp_dyn`;
CREATE TABLE IF NOT EXISTS `sys_ldap_imp_dyn` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ldap_domain_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `real_domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_username` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_functions` text COLLATE utf8_unicode_ci NOT NULL,
  `ldap_error_msg` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_error_reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_in_transport` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `ldap_in_active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `ldap_in_mail_db` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `ldap_not_longer_in_mail_db` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `ldap_in_ignore` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_ldap_tmp
DROP TABLE IF EXISTS `sys_ldap_tmp`;
CREATE TABLE IF NOT EXISTS `sys_ldap_tmp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(60) COLLATE utf8_unicode_ci DEFAULT '',
  `email` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_account_suffix` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_listing
DROP TABLE IF EXISTS `sys_listing`;
CREATE TABLE IF NOT EXISTS `sys_listing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `msgid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `from_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to_domain` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ipaddress` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `request` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('n','y') COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_logs
DROP TABLE IF EXISTS `sys_logs`;
CREATE TABLE IF NOT EXISTS `sys_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `severity` int(11) DEFAULT NULL,
  `date` varchar(32) DEFAULT NULL,
  `username` varchar(32) DEFAULT NULL,
  `ipaddr` varchar(64) DEFAULT NULL,
  `command` varchar(128) DEFAULT '0',
  `details` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_log_config
DROP TABLE IF EXISTS `sys_log_config`;
CREATE TABLE IF NOT EXISTS `sys_log_config` (
  `setting` text COLLATE utf8_unicode_ci,
  `value` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_mail_firewall
DROP TABLE IF EXISTS `sys_mail_firewall`;
CREATE TABLE IF NOT EXISTS `sys_mail_firewall` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sys_userid` int(11) unsigned NOT NULL DEFAULT '0',
  `sys_groupid` int(11) unsigned NOT NULL DEFAULT '0',
  `sys_perm_user` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sys_perm_group` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sys_perm_other` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `server_id` int(11) unsigned NOT NULL DEFAULT '0',
  `block_ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `block_ip_mask` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tcp_port` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `udp_port` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_mail_log
DROP TABLE IF EXISTS `sys_mail_log`;
CREATE TABLE IF NOT EXISTS `sys_mail_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `log_date` datetime NOT NULL,
  `line` text NOT NULL,
  `hostname` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `process` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `process_type` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `process_id` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `message_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `requeue_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `to_address` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `orig_to` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `from_address` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `to_domain` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `from_domain` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `relay` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ip_add` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `delay` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `delays` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `dsn` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `status` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `info` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `size` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `rule` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `error_code` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `status_code` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `status_info` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `reject_by` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `reject_info` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `proto` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `helo` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `port` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `log_date` (`log_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_mcp_rules
DROP TABLE IF EXISTS `sys_mcp_rules`;
CREATE TABLE IF NOT EXISTS `sys_mcp_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enabled` int(11) DEFAULT NULL,
  `rulename` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ruledesc` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ruletype` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pattern` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_ms_rulessets
DROP TABLE IF EXISTS `sys_ms_rulessets`;
CREATE TABLE IF NOT EXISTS `sys_ms_rulessets` (
  `variable_name` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_ms_rule_sets
DROP TABLE IF EXISTS `sys_ms_rule_sets`;
CREATE TABLE IF NOT EXISTS `sys_ms_rule_sets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rname` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `0direction` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `1target` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `2and` int(1) NOT NULL DEFAULT '0',
  `3anddirection` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `4andtarget` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `isdefault` int(1) NOT NULL DEFAULT '0',
  `action` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_notify_conf
DROP TABLE IF EXISTS `sys_notify_conf`;
CREATE TABLE IF NOT EXISTS `sys_notify_conf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registration_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notification_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mail_server` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `server_port` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `growl` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'n',
  `smtp_ssl` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `starttls` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `from_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_phishing_bad_sites_conf_master
DROP TABLE IF EXISTS `sys_phishing_bad_sites_conf_master`;
CREATE TABLE IF NOT EXISTS `sys_phishing_bad_sites_conf_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` text COLLATE utf8_unicode_ci,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_phishing_sites
DROP TABLE IF EXISTS `sys_phishing_sites`;
CREATE TABLE IF NOT EXISTS `sys_phishing_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `safe` enum('y','n') COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_policyd_config
DROP TABLE IF EXISTS `sys_policyd_config`;
CREATE TABLE IF NOT EXISTS `sys_policyd_config` (
  `variable_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_pop3_conf
DROP TABLE IF EXISTS `sys_pop3_conf`;
CREATE TABLE IF NOT EXISTS `sys_pop3_conf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `server` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `port` int(11) DEFAULT '110',
  `username` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `domain` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_quarantine_message
DROP TABLE IF EXISTS `sys_quarantine_message`;
CREATE TABLE IF NOT EXISTS `sys_quarantine_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `html` text COLLATE utf8_unicode_ci,
  `domain` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_rbl_blacklist
DROP TABLE IF EXISTS `sys_rbl_blacklist`;
CREATE TABLE IF NOT EXISTS `sys_rbl_blacklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `relay_ip` text CHARACTER SET latin1,
  `blocklist` text CHARACTER SET latin1,
  `info` text CHARACTER SET latin1,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_rbl_config
DROP TABLE IF EXISTS `sys_rbl_config`;
CREATE TABLE IF NOT EXISTS `sys_rbl_config` (
  `variable_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_rbl_list
DROP TABLE IF EXISTS `sys_rbl_list`;
CREATE TABLE IF NOT EXISTS `sys_rbl_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rbl` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_rbl_lookup
DROP TABLE IF EXISTS `sys_rbl_lookup`;
CREATE TABLE IF NOT EXISTS `sys_rbl_lookup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `relay` text,
  `relay_ip` text,
  `country` text,
  `from_address` text,
  `tld` text,
  `created` text,
  `action` text,
  `to_address` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_rbl_override
DROP TABLE IF EXISTS `sys_rbl_override`;
CREATE TABLE IF NOT EXISTS `sys_rbl_override` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_recover_password
DROP TABLE IF EXISTS `sys_recover_password`;
CREATE TABLE IF NOT EXISTS `sys_recover_password` (
  `email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `recovery_code` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `recovery_id` int(11) NOT NULL AUTO_INCREMENT,
  `datestamp` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `client_ip` mediumtext COLLATE utf8_unicode_ci,
  `agent` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`recovery_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_relay_country
DROP TABLE IF EXISTS `sys_relay_country`;
CREATE TABLE IF NOT EXISTS `sys_relay_country` (
  `id` int(11) DEFAULT NULL,
  `to_address` text COLLATE utf8_unicode_ci,
  `to_domain` text COLLATE utf8_unicode_ci,
  `from_address` text COLLATE utf8_unicode_ci,
  `from_domain` text COLLATE utf8_unicode_ci,
  `country` text COLLATE utf8_unicode_ci,
  `relay` text COLLATE utf8_unicode_ci,
  `active` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'y'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_rlog_config
DROP TABLE IF EXISTS `sys_rlog_config`;
CREATE TABLE IF NOT EXISTS `sys_rlog_config` (
  `variable_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_sasl_user
DROP TABLE IF EXISTS `sys_sasl_user`;
CREATE TABLE IF NOT EXISTS `sys_sasl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_sa_rules
DROP TABLE IF EXISTS `sys_sa_rules`;
CREATE TABLE IF NOT EXISTS `sys_sa_rules` (
  `id` int(11) DEFAULT NULL,
  `num` int(11) DEFAULT NULL,
  `header` text COLLATE utf8_unicode_ci,
  `describe` text COLLATE utf8_unicode_ci,
  `score` text COLLATE utf8_unicode_ci,
  `type` enum('bad','good') COLLATE utf8_unicode_ci DEFAULT 'bad',
  `active` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'y'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_slog_data
DROP TABLE IF EXISTS `sys_slog_data`;
CREATE TABLE IF NOT EXISTS `sys_slog_data` (
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sid` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `rid` mediumtext COLLATE utf8_unicode_ci,
  `size` bigint(20) DEFAULT '0',
  `from_address` mediumtext COLLATE utf8_unicode_ci,
  `to_address` mediumtext COLLATE utf8_unicode_ci,
  `clientip` mediumtext COLLATE utf8_unicode_ci,
  `hostname` mediumtext COLLATE utf8_unicode_ci,
  `source` mediumtext COLLATE utf8_unicode_ci,
  `subject` mediumtext COLLATE utf8_unicode_ci,
  `report` mediumtext COLLATE utf8_unicode_ci,
  `rule` mediumtext COLLATE utf8_unicode_ci,
  `stype` mediumtext COLLATE utf8_unicode_ci,
  KEY `slog_sid_idx` (`sid`),
  KEY `slog_timestamp_idx` (`timestamp`),
  KEY `slog_from_address_idx` (`from_address`(200)),
  KEY `slog_to_address_idx` (`to_address`(200)),
  KEY `slog_clientip_idx` (`clientip`(20))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_static_routes
DROP TABLE IF EXISTS `sys_static_routes`;
CREATE TABLE IF NOT EXISTS `sys_static_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `netmask` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `interface` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ipaddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metric` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `network` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `command` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_stats_cpuload
DROP TABLE IF EXISTS `sys_stats_cpuload`;
CREATE TABLE IF NOT EXISTS `sys_stats_cpuload` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `load1` decimal(5,2) NOT NULL,
  `load5` decimal(5,2) NOT NULL,
  `load15` decimal(5,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_stats_disk
DROP TABLE IF EXISTS `sys_stats_disk`;
CREATE TABLE IF NOT EXISTS `sys_stats_disk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `mount` varchar(255) DEFAULT NULL,
  `used` varchar(255) NOT NULL,
  `free` varchar(255) DEFAULT NULL,
  `total` varchar(255) DEFAULT NULL,
  `percent_used` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_stats_memload
DROP TABLE IF EXISTS `sys_stats_memload`;
CREATE TABLE IF NOT EXISTS `sys_stats_memload` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `used` varchar(255) NOT NULL,
  `free` varchar(255) DEFAULT NULL,
  `total` varchar(255) DEFAULT NULL,
  `percent_used` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_stats_network
DROP TABLE IF EXISTS `sys_stats_network`;
CREATE TABLE IF NOT EXISTS `sys_stats_network` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `interface` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `transmit` varchar(255) DEFAULT NULL,
  `receive` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_stats_swap
DROP TABLE IF EXISTS `sys_stats_swap`;
CREATE TABLE IF NOT EXISTS `sys_stats_swap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `used` varchar(255) NOT NULL,
  `free` varchar(255) DEFAULT NULL,
  `total` varchar(255) DEFAULT NULL,
  `percent_used` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_time_zones
DROP TABLE IF EXISTS `sys_time_zones`;
CREATE TABLE IF NOT EXISTS `sys_time_zones` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `time_zones` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_tls_domains
DROP TABLE IF EXISTS `sys_tls_domains`;
CREATE TABLE IF NOT EXISTS `sys_tls_domains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `smtp` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `policy` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_tls_log_level
DROP TABLE IF EXISTS `sys_tls_log_level`;
CREATE TABLE IF NOT EXISTS `sys_tls_log_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `option` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('N','Y') COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`value`),
  UNIQUE KEY `description` (`option`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_tls_policy_type
DROP TABLE IF EXISTS `sys_tls_policy_type`;
CREATE TABLE IF NOT EXISTS `sys_tls_policy_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `option` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('N','Y') COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`value`),
  UNIQUE KEY `description` (`option`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_trusted_networks
DROP TABLE IF EXISTS `sys_trusted_networks`;
CREATE TABLE IF NOT EXISTS `sys_trusted_networks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `network` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_urls
DROP TABLE IF EXISTS `sys_urls`;
CREATE TABLE IF NOT EXISTS `sys_urls` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url` text COLLATE utf8_unicode_ci,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(60) COLLATE utf8_unicode_ci DEFAULT '',
  `password` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `fullname` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` enum('A','D','U','R','H') COLLATE utf8_unicode_ci DEFAULT NULL,
  `quarantine_report` tinyint(1) DEFAULT '0',
  `spamscore` tinyint(4) DEFAULT '0',
  `highspamscore` tinyint(4) DEFAULT '0',
  `noscan` tinyint(1) DEFAULT '0',
  `quarantine_rcpt` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `confirmation` char(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reg_date` datetime DEFAULT NULL,
  `mod_date` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `last_login` datetime NOT NULL,
  `num_logins` int(11) NOT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `memo` text COLLATE utf8_unicode_ci NOT NULL,
  `ldap_account_suffix` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `proxyaddresses` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'LOCAL',
  `ldap_uid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_uid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lang` int(11) NOT NULL DEFAULT '1',
  `portaluser` int(1) NOT NULL,
  `enabled` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.user_auth_type
DROP TABLE IF EXISTS `user_auth_type`;
CREATE TABLE IF NOT EXISTS `user_auth_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.user_filters
DROP TABLE IF EXISTS `user_filters`;
CREATE TABLE IF NOT EXISTS `user_filters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `filter` mediumtext COLLATE utf8_unicode_ci,
  `verify_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `active` enum('N','Y') COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `user_filters_username_idx` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.user_type
DROP TABLE IF EXISTS `user_type`;
CREATE TABLE IF NOT EXISTS `user_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('N','Y') COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`),
  UNIQUE KEY `description` (`description`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.vacation
DROP TABLE IF EXISTS `vacation`;
CREATE TABLE IF NOT EXISTS `vacation` (
  `email` varchar(255) NOT NULL DEFAULT '',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `body` text NOT NULL,
  `cache` text NOT NULL,
  `domain` varchar(255) NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`email`),
  KEY `email` (`email`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.vacation_notification
DROP TABLE IF EXISTS `vacation_notification`;
CREATE TABLE IF NOT EXISTS `vacation_notification` (
  `on_vacation` varchar(255) NOT NULL,
  `notified` varchar(255) NOT NULL,
  `notified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`on_vacation`,`notified`),
  CONSTRAINT `vacation_notification_ibfk_1` FOREIGN KEY (`on_vacation`) REFERENCES `vacation` (`email`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.verified_online
DROP TABLE IF EXISTS `verified_online`;
CREATE TABLE IF NOT EXISTS `verified_online` (
  `phish_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phish_detail_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submission_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verified` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verification_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `online` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `target` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.virtual_sender_access
DROP TABLE IF EXISTS `virtual_sender_access`;
CREATE TABLE IF NOT EXISTS `virtual_sender_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `access` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.whitelist
DROP TABLE IF EXISTS `whitelist`;
CREATE TABLE IF NOT EXISTS `whitelist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `to_address` mediumtext COLLATE utf8_unicode_ci,
  `to_domain` mediumtext COLLATE utf8_unicode_ci,
  `from_address` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `whitelist_uniq` (`to_address`(100),`from_address`(100))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping database structure for opendmarc
DROP DATABASE IF EXISTS `opendmarc`;
CREATE DATABASE IF NOT EXISTS `opendmarc` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `opendmarc`;


-- Dumping structure for table opendmarc.domains
DROP TABLE IF EXISTS `domains`;
CREATE TABLE IF NOT EXISTS `domains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `firstseen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table opendmarc.ipaddr
DROP TABLE IF EXISTS `ipaddr`;
CREATE TABLE IF NOT EXISTS `ipaddr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `addr` varchar(64) NOT NULL,
  `firstseen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `addr` (`addr`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table opendmarc.messages
DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `jobid` varchar(128) NOT NULL,
  `reporter` int(10) unsigned NOT NULL,
  `policy` tinyint(3) unsigned NOT NULL,
  `disp` tinyint(3) unsigned NOT NULL,
  `ip` int(10) unsigned NOT NULL,
  `env_domain` int(10) unsigned NOT NULL,
  `from_domain` int(10) unsigned NOT NULL,
  `policy_domain` int(10) unsigned NOT NULL,
  `spf` tinyint(3) unsigned NOT NULL,
  `align_dkim` tinyint(3) unsigned NOT NULL,
  `align_spf` tinyint(3) unsigned NOT NULL,
  `sigcount` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `reporter` (`reporter`,`date`,`jobid`),
  KEY `date` (`date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table opendmarc.reporters
DROP TABLE IF EXISTS `reporters`;
CREATE TABLE IF NOT EXISTS `reporters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `firstseen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table opendmarc.requests
DROP TABLE IF EXISTS `requests`;
CREATE TABLE IF NOT EXISTS `requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` int(11) NOT NULL,
  `repuri` varchar(255) NOT NULL,
  `adkim` tinyint(4) NOT NULL,
  `aspf` tinyint(4) NOT NULL,
  `policy` tinyint(4) NOT NULL,
  `spolicy` tinyint(4) NOT NULL,
  `pct` tinyint(4) NOT NULL,
  `locked` tinyint(4) NOT NULL,
  `firstseen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastsent` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `domain` (`domain`),
  KEY `lastsent` (`lastsent`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table opendmarc.signatures
DROP TABLE IF EXISTS `signatures`;
CREATE TABLE IF NOT EXISTS `signatures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` int(11) NOT NULL,
  `domain` int(11) NOT NULL,
  `pass` tinyint(4) NOT NULL,
  `error` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `message` (`message`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping database structure for sa_bayes
DROP DATABASE IF EXISTS `sa_bayes`;
CREATE DATABASE IF NOT EXISTS `sa_bayes` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `sa_bayes`;


-- Dumping structure for table sa_bayes.awl
DROP TABLE IF EXISTS `awl`;
CREATE TABLE IF NOT EXISTS `awl` (
  `username` varchar(100) NOT NULL DEFAULT '',
  `email` varbinary(255) NOT NULL DEFAULT '',
  `ip` varchar(40) NOT NULL DEFAULT '',
  `count` int(11) NOT NULL DEFAULT '0',
  `totscore` float NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`username`,`email`,`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table sa_bayes.bayes_expire
DROP TABLE IF EXISTS `bayes_expire`;
CREATE TABLE IF NOT EXISTS `bayes_expire` (
  `id` int(11) NOT NULL DEFAULT '0',
  `runtime` int(11) NOT NULL DEFAULT '0',
  KEY `bayes_expire_idx1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table sa_bayes.bayes_global_vars
DROP TABLE IF EXISTS `bayes_global_vars`;
CREATE TABLE IF NOT EXISTS `bayes_global_vars` (
  `variable` varchar(30) NOT NULL DEFAULT '',
  `value` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`variable`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table sa_bayes.bayes_seen
DROP TABLE IF EXISTS `bayes_seen`;
CREATE TABLE IF NOT EXISTS `bayes_seen` (
  `id` int(11) NOT NULL DEFAULT '0',
  `msgid` varchar(200) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `flag` char(1) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`msgid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table sa_bayes.bayes_token
DROP TABLE IF EXISTS `bayes_token`;
CREATE TABLE IF NOT EXISTS `bayes_token` (
  `id` int(11) NOT NULL DEFAULT '0',
  `token` binary(5) NOT NULL DEFAULT '\0\0\0\0\0',
  `spam_count` int(11) NOT NULL DEFAULT '0',
  `ham_count` int(11) NOT NULL DEFAULT '0',
  `atime` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`token`),
  KEY `bayes_token_idx1` (`id`,`atime`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table sa_bayes.bayes_vars
DROP TABLE IF EXISTS `bayes_vars`;
CREATE TABLE IF NOT EXISTS `bayes_vars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL DEFAULT '',
  `spam_count` int(11) NOT NULL DEFAULT '0',
  `ham_count` int(11) NOT NULL DEFAULT '0',
  `token_count` int(11) NOT NULL DEFAULT '0',
  `last_expire` int(11) NOT NULL DEFAULT '0',
  `last_atime_delta` int(11) NOT NULL DEFAULT '0',
  `last_expire_reduce` int(11) NOT NULL DEFAULT '0',
  `oldest_token_age` int(11) NOT NULL DEFAULT '2147483647',
  `newest_token_age` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `bayes_vars_idx1` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table sa_bayes.connect
DROP TABLE IF EXISTS `connect`;
CREATE TABLE IF NOT EXISTS `connect` (
  `sender_name` varchar(64) NOT NULL,
  `sender_domain` varchar(255) NOT NULL,
  `src` varchar(39) NOT NULL,
  `rcpt` varchar(255) NOT NULL,
  `first_seen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `connect_idx` (`src`,`sender_domain`,`sender_name`) USING BTREE,
  KEY `connect_fseen` (`first_seen`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping database structure for sqlgrey
DROP DATABASE IF EXISTS `sqlgrey`;
CREATE DATABASE IF NOT EXISTS `sqlgrey` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `sqlgrey`;


-- Dumping structure for table sqlgrey.config
DROP TABLE IF EXISTS `config`;
CREATE TABLE IF NOT EXISTS `config` (
  `parameter` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`parameter`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table sqlgrey.connect
DROP TABLE IF EXISTS `connect`;
CREATE TABLE IF NOT EXISTS `connect` (
  `sender_name` varchar(64) NOT NULL,
  `sender_domain` varchar(255) NOT NULL,
  `src` varchar(39) NOT NULL,
  `rcpt` varchar(255) NOT NULL,
  `first_seen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `connect_idx` (`src`,`sender_domain`,`sender_name`) USING BTREE,
  KEY `connect_fseen` (`first_seen`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table sqlgrey.domain_awl
DROP TABLE IF EXISTS `domain_awl`;
CREATE TABLE IF NOT EXISTS `domain_awl` (
  `sender_domain` varchar(255) NOT NULL,
  `src` varchar(39) NOT NULL,
  `first_seen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_seen` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`src`,`sender_domain`),
  KEY `domain_awl_lseen` (`last_seen`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table sqlgrey.from_awl
DROP TABLE IF EXISTS `from_awl`;
CREATE TABLE IF NOT EXISTS `from_awl` (
  `sender_name` varchar(64) NOT NULL,
  `sender_domain` varchar(255) NOT NULL,
  `src` varchar(39) NOT NULL,
  `first_seen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_seen` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`src`,`sender_domain`,`sender_name`),
  KEY `from_awl_lseen` (`last_seen`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table sqlgrey.optin_domain
DROP TABLE IF EXISTS `optin_domain`;
CREATE TABLE IF NOT EXISTS `optin_domain` (
  `domain` varchar(255) NOT NULL,
  PRIMARY KEY (`domain`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table sqlgrey.optin_email
DROP TABLE IF EXISTS `optin_email`;
CREATE TABLE IF NOT EXISTS `optin_email` (
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table sqlgrey.optout_domain
DROP TABLE IF EXISTS `optout_domain`;
CREATE TABLE IF NOT EXISTS `optout_domain` (
  `domain` varchar(255) NOT NULL,
  PRIMARY KEY (`domain`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table sqlgrey.optout_email
DROP TABLE IF EXISTS `optout_email`;
CREATE TABLE IF NOT EXISTS `optout_email` (
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping database structure for webtoken
DROP DATABASE IF EXISTS `webtoken`;
CREATE DATABASE IF NOT EXISTS `webtoken` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `webtoken`;


-- Dumping structure for table webtoken.spamlog
DROP TABLE IF EXISTS `spamlog`;
CREATE TABLE IF NOT EXISTS `spamlog` (
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id` mediumtext COLLATE utf8_unicode_ci,
  `size` bigint(20) DEFAULT '0',
  `from_address` mediumtext COLLATE utf8_unicode_ci,
  `from_domain` mediumtext COLLATE utf8_unicode_ci,
  `to_address` mediumtext COLLATE utf8_unicode_ci,
  `to_domain` mediumtext COLLATE utf8_unicode_ci,
  `subject` mediumtext COLLATE utf8_unicode_ci,
  `clientip` mediumtext COLLATE utf8_unicode_ci,
  `archive` mediumtext COLLATE utf8_unicode_ci,
  `isspam` tinyint(1) DEFAULT '0',
  `ishighspam` tinyint(1) DEFAULT '0',
  `issaspam` tinyint(1) DEFAULT '0',
  `isrblspam` tinyint(1) DEFAULT '0',
  `isfp` tinyint(1) DEFAULT '0',
  `isfn` tinyint(1) DEFAULT '0',
  `spamwhitelisted` tinyint(1) DEFAULT '0',
  `spamblacklisted` tinyint(1) DEFAULT '0',
  `sascore` decimal(7,2) DEFAULT '0.00',
  `spamreport` mediumtext COLLATE utf8_unicode_ci,
  `virusinfected` tinyint(1) DEFAULT '0',
  `nameinfected` tinyint(1) DEFAULT '0',
  `otherinfected` tinyint(1) DEFAULT '0',
  `report` mediumtext COLLATE utf8_unicode_ci,
  `ismcp` tinyint(1) DEFAULT '0',
  `ishighmcp` tinyint(1) DEFAULT '0',
  `issamcp` tinyint(1) DEFAULT '0',
  `mcpwhitelisted` tinyint(1) DEFAULT '0',
  `mcpblacklisted` tinyint(1) DEFAULT '0',
  `mcpsascore` decimal(7,2) DEFAULT '0.00',
  `mcpreport` mediumtext COLLATE utf8_unicode_ci,
  `hostname` mediumtext COLLATE utf8_unicode_ci,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `headers` mediumtext COLLATE utf8_unicode_ci,
  `quarantined` tinyint(1) DEFAULT '0',
  KEY `maillog_datetime_idx` (`date`,`time`),
  KEY `maillog_id_idx` (`id`(20)),
  KEY `maillog_clientip_idx` (`clientip`(20)),
  KEY `maillog_from_idx` (`from_address`(200)),
  KEY `maillog_to_idx` (`to_address`(200)),
  KEY `maillog_host` (`hostname`(30)),
  KEY `from_domain_idx` (`from_domain`(50)),
  KEY `to_domain_idx` (`to_domain`(50)),
  KEY `maillog_quarantined` (`quarantined`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table webtoken.tokens
DROP TABLE IF EXISTS `tokens`;
CREATE TABLE IF NOT EXISTS `tokens` (
  `token` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `id` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `datestamp` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
