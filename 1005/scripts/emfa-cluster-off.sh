#!/bin/bash
echo "[mysqld]
bind-address=127.0.0.1
datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock
user=mysql
# Default to using old password format for compatibility with mysql 3.x
# clients (those using the mysqlclient10 compatibility package).
old_passwords=1
max_connections = 250
key_buffer_size = 100M
query_cache_size = 32M
tmp_table_size = 60M
max_heap_table_size = 60M
join_buffer_size = 1M
innodb_buffer_pool_size = 80M
thread_cache_size = 4

[mysqld_safe]
log-error=/var/log/mysqld.log
pid-file=/var/run/mysqld/mysqld.pid
">/etc/my.cnf

MYSQLROOTPWD=`grep MYSQLROOTPWD /etc/DB-Config | sed 's/.*://'`

#Remove other node settings from ms_config table
hn=`hostname`
echo "stop slave; reset slave; reset master;" | mysql -u root --password=$MYSQLROOTPWD 2>&1
echo "USE mailscanner; DELETE FROM ms_config WHERE hostname!='$hn';" | mysql -u root --password=$MYSQLROOTPWD 2>&1
echo "USE cluster; delete from settings;" | mysql -u root --password=$MYSQLROOTPWD 2>&1
echo "USE cluster; delete from test;" | mysql -u root --password=$MYSQLROOTPWD 2>&1

rm -rf /var/lib/mysql/master.info
service mysqld restart
/usr/local/sbin/emfa-cluster-lsyncd off
rm -rf /etc/lsyncd.conf

#Remove pwdless ssh keys
rm -rf /root/.ssh

#Remove sshd auth from hosts.allow
sed -i "/^sshd:/d" /etc/hosts.allow
