-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.1.73-log - Source distribution
-- Server OS:                    redhat-linux-gnu
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table mailscanner.sys_ldap_imp_dyn
DROP TABLE IF EXISTS `sys_ldap_imp_dyn`;
CREATE TABLE IF NOT EXISTS `sys_ldap_imp_dyn` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ldap_domain_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `real_domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_username` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_functions` text COLLATE utf8_unicode_ci NOT NULL,
  `ldap_error_msg` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_error_reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_in_transport` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `ldap_in_active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `ldap_in_mail_db` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `ldap_not_longer_in_mail_db` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `ldap_in_ignore` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
