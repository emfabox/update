#!/bin/bash
########################################################################
# EMFABOX UPGRADE SCRIPT                                               #
#                                                                      #
# V1.a                                                                 #
########################################################################
# Copyright (C) 2014, 2015  http://www.cycomptec.com                   #
########################################################################
#
# Build date 30-11-2015
#
VERSION="2.0.0.1"
OLD_VERSION="2.0.0.0"
OLD_BACKUP="1.0.0.9"
logdir="/var/log/emfa"
UPDATEDIR="/tmp/EMFA-Update"
yumexclude="kernel* mysql* postfix* mailscanner* clamav* clamd* open-vm-tools*"

##########################################################################################################################
function emfa_update() {

source /etc/emfa/variables.conf

echo "Starting update to EMFABox $VERSION"

##### Backup Phase #####
#/usr/local/sbin/EMFA-Backup -backup

##### Commit Phase #####

#EMFABOX_AUTOUPDATE="no"
#sed -i '/^EMFABOX_MEMORY/a EMFABOX_AUTOUPDATE="no"' /etc/emfa/variables.conf

#update dirs
if [ ! -d /opt/emfa/update ] ; then
    mkdir -p /opt/emfa/update
fi

if [ ! -d /opt/emfa/staging ] ; then
    mkdir -p /opt/emfa/staging
fi


# rkhunter php-pecl-mailparse
yum install -y rkhunter php-pecl-mailparse perl-Crypt-SSLeay

rpm -Uhv http://yum.spacewalkproject.org/2.3-client/RHEL/6/x86_64/spacewalk-client-repo-2.3-2.el6.noarch.rpm

yum install -y yum-rhn-plugin 





#sudoers.d

cd /etc/sudoers.d



echo "apache ALL=(root) NOPASSWD: /usr/local/bin/sa-learn">>/etc/sudoers.d/EMFA-Services

visudo -c -f /etc/sudoers.d/EMFA-Services > /tmp/visudo



########
# MailScanner

sed -i "/^Quarantine Infections =/ c\Quarantine Infections = yes" /etc/MailScanner/MailScanner.conf
sed -i "/^Quarantine Silent Viruses =/ c\Quarantine Silent Viruses = yes" /etc/MailScanner/MailScanner.conf
sed -i "/^Maximum Archive Depth =/ c\Maximum Archive Depth = 2" /etc/MailScanner/MailScanner.conf

#SELECT @rownum:=@rownum+1 num, rule FROM ms_rulesets, (SELECT @rownum:=0) r WHERE name = ?
sed -i '/^SQL Ruleset =/ c\SQL Ruleset = SELECT @rownum:=@rownum+1 num, rule FROM ms_rulesets, (SELECT @rownum:=0) r WHERE name=?' /etc/MailScanner/MailScanner.conf


# cgi-script
#release-msg.cgi
#https://bitbucket.org/emfabox/update/raw/1de15600aa4c97a8e9a8bee8221f195a6e22ba0a/2000/var/www/cgi-bin/release-msg.cgi



# sql
MYSQLROOTPWD=`grep MYSQLROOTPWD /etc/DB-Config | sed 's/.*://'`
now=$(date +"%m_%d_%Y")
mysqldump -u root -p"${MYSQLROOTPWD}" mailscanner > /var/www/emfa_backup/${OLD_VERSION}_$now.sql
cd  /var/www/emfa_backup/
tar -czvf  ${OLD_VERSION}_$now.sql.tar.gz ${OLD_VERSION}_$now.sql
rm -f  /var/www/emfa_backup/*.sql

cd /opt/emfa/msdbconf/

# sa_user database 
echo "CREATE TABLE IF NOT EXISTS \`sys_rule_id\` (\`id\` INT(11) NOT NULL AUTO_INCREMENT,\`num\` VARCHAR(100) NOT NULL COLLATE 'utf8_unicode_ci',PRIMARY KEY (\`id\`)) COLLATE='utf8_unicode_ci' ENGINE=MyISAM;">/opt/emfa/msdbconf/ms_update.sql
echo "ALTER TABLE \`sa_users\` ADD COLUMN \`maxzipdepth\` VARCHAR(2) NULL DEFAULT '2' COLLATE 'utf8_unicode_ci' AFTER \`high_spam_header_txt\`;" >>/opt/emfa/msdbconf/ms_update.sql
echo "ALTER TABLE \`sys_listing\` ADD COLUMN \`datetime\` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER \`request\`;">>/opt/emfa/msdbconf/ms_update.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/ms_update.sql


 # switch from 8888 to 9999 for default rulesets #
echo "UPDATE ms_config SET \`value\`='virusscan.customize' WHERE  \`external\`='virusscan'; ">/opt/emfa/msdbconf/ms_config.sql
echo "DELETE FROM ms_rulesets WHERE num='8888';">>/opt/emfa/msdbconf/ms_config.sql
echo "DELETE FROM ms_rulesets WHERE name='spammodifysubject';">>/opt/emfa/msdbconf/ms_config.sql
echo "REPLACE INTO ms_rulesets (\`name\`, \`num\`, \`rule\`) VALUES ('textsigs', '9999', 'To: default /etc/MailScanner/reports/en/inline.sig.out.txt');">>/opt/emfa/msdbconf/ms_config.sql
echo "REPLACE INTO ms_rulesets (\`name\`, \`num\`, \`rule\`) VALUES ('htmlsigs', '9999', 'To: default /etc/MailScanner/reports/en/inline.sig.out.html');">>/opt/emfa/msdbconf/ms_config.sql
echo "REPLACE INTO ms_rulesets (\`name\`, \`num\`, \`rule\`) VALUES ('cleansigs', '9999', 'To: default yes');">>/opt/emfa/msdbconf/ms_config.sql
echo "REPLACE INTO ms_rulesets (\`name\`, \`num\`, \`rule\`) VALUES ('spammodifysubject', '9999', 'To: default yes');">>/opt/emfa/msdbconf/ms_config.sql
echo "REPLACE INTO ms_rulesets (\`name\`, \`num\`, \`rule\`) VALUES ('maxzipdepth', '9999', 'To: default 2');">>/opt/emfa/msdbconf/ms_config.sql
echo "REPLACE INTO ms_rulesets (\`name\`, \`num\`, \`rule\`) VALUES ('virusscan', '9999', 'FromOrTo: default yes');">>/opt/emfa/msdbconf/ms_config.sql
/usr/bin/mysql  -hlocalhost -uroot -p"${MYSQLROOTPWD}" mailscanner </opt/emfa/msdbconf/ms_config.sql

   
#
#postconf: warning: /etc/postfix/main.cf: unused parameter: greylisting=yes
sed -i 's/greylisting = yes/#greylisting = yes/g' /etc/postfix/main.cf    
sed -i 's/greylisting = yes/#greylisting = yes/g' /etc/postfix-out/main.cf      
#postconf: warning: /etc/postfix/main.cf: unused parameter: policy_time_limit=360
sed -i 's/policy_time_limit = 3600/#policy_time_limit = 3600/g' /etc/postfix/main.cf
sed -i 's/policy_time_limit = 3600/#policy_time_limit = 3600/g' /etc/postfix-out/main.cf                                                                                                                                                              
#postconf: warning: /etc/postfix/main.cf: unused parameter: permit_sasl_authenticated=yes
sed -i 's/permit_sasl_authenticated = yes/#permit_sasl_authenticated = yes/g' /etc/postfix/main.cf     
sed -i 's/permit_sasl_authenticated = yes/#permit_sasl_authenticated = yes/g' /etc/postfix-out/main.cf 


# php timezone
DATETZ=`head -n 1 /etc/sysconfig/clock| sed 's/ZONE=//g'`
sed -i "s#;date.timezone =#date.timezone =${DATETZ}#" /etc/php.ini
sed -i 's/max_execution_time = 30/max_execution_time = 60/g' /etc/php.ini


#copy license 
if [ -f  /var/www/html/license.php ] ; then
	cp -f /var/www/html/license.php /tmp/
fi

# remove old backup
if [ -f  /var/www/emfa_backup/html-v-${OLD_BACKUP}.tar.gz ] ; then
	rm -f /var/www/emfa_backup/html-v-${OLD_BACKUP}.tar.gz
fi


#backup old version
cd /var/www/
tar -czvf html-v-${OLD_VERSION}.tar.gz html/*
mv -f html-v-${OLD_VERSION}.tar.gz  /var/www/emfa_backup/

cd /var/www/html/
rm -rf *

cd /opt/emfa/staging/
/usr/bin/wget --no-check-certificate -O /opt/emfa/staging/html.run https://s3-us-west-2.amazonaws.com/emfabox/html/html.run

chmod +x  /opt/emfa/staging/html.run 

./html.run --target /var/www/html/

rm -f  /opt/emfa/staging/html.run

# set permissions
# run script
/usr/local/sbin/set_permissions.sh

#https://s3-us-west-2.amazonaws.com/emfabox/demo/license.php
/usr/bin/wget --no-check-certificate -O /var/www/html/license.php https://s3-us-west-2.amazonaws.com/emfabox/release/AWS/demo/license.php

#move license back
if [ -f  /tmp/license.php ] ; then
	mv -f /tmp/license.php  /var/www/html/license.php
fi

#exclude update 

CHECKEXCLUDE=$(grep "^exclude" /etc/yum.conf)
if [[ -z $CHECKEXCLUDE ]]; then
echo "exclude=$yumexclude" >> /etc/yum.conf
fi

## remove old clamav database

if [ -f  /var/lib/clamav/daily.cld ] ; then
rm -f /var/lib/clamav/daily.cld
fi


}


##########################################################################################################################
### GLOBAL FUNCTIONS 
function emfa_test_host () {

if [ -f /opt/emfa/id/ping_google ] ; then

rm -rf  /opt/emfa/id/ping_google

fi;
touch /opt/emfa/id/ping_google
clear 

echo -----testing host: "$1":"$2" via ping-----
ping -c1 -W1 "$2" | grep 'time=' > /dev/null
#echo $?
if [ $? -eq 0 ]
then
echo OK: "$1" is up
sleep 2s
clear
else
echo -e "\e[1;31mInternet is down or google.com is not accessible so we can not download ... Aborting.\e[0m"; echo;
sleep 2s
exit 1
fi

}

## CHECK OS
function emfa_check_os () {

MACHINE_TYPE=`uname -m`

if [ ${MACHINE_TYPE} == 'x86_64' ]; then

    # 64-bit 
    printf "\n"
    printf "\e[1;42m 64-bit OS detected ...\e[0m";  printf "\n"
    printf "\n"
    
    sleep 2s
    
else 

echo -e "\e[1;31m 32-bit OS detected ... Aborting.\e[0m"; echo;

sleep 2s  
  
  exit 1
  
fi

}

## check MD5 
function emfa_check_md5() {
  # Grab md5 from file
  checksum1=$(cat $UPDATEDIR/$UPDATE.md5 | awk '{print $1}')
  # Calculate md5
  checksum2=$(md5sum $UPDATEDIR/$UPDATE | awk '{print $1}')
  if [[ "$checksum1" != "$checksum2" ]]; then
    echo "Fatal:  $UPDATE md5 checksum does not match!  Aborting..."
    abort
  fi
}

## abort install
function emfa_abort() {
  [[ -n $UPDATEDIR ]] && rm -rf $UPDATEDIR
  
  echo "Incorrect EMFABox version!"
  echo "Update to Version $VERSION FAILED.  Updates cancelled."
  echo "Please visit http://emfabox.com for more information."
  exit 1
}

## finish
function emfa_finish_update() {
  # Write the latest version number
  echo "$VERSION" > /etc/EMFA-Version

  cd /tmp
  [[ -n $UPDATEDIR ]] && rm -rf $UPDATEDIR
  
##re create dir's

if [ ! -d /opt/emfa/update ] ; then
    mkdir -p /opt/emfa/update
fi
chmod 755 /opt/emfa/update
chown apache:apache /opt/emfa/update

if [ ! -d /opt/emfa/staging ] ; then
    mkdir -p /opt/emfa/staging
fi
chmod 755 /opt/emfa/staging
chown apache:apache /opt/emfa/staging
##  
  

  echo "All done"
  echo "If your system does not return to a command prompt"
  echo "you can now safely press ctrl-c to abort logsave"
  exit 0
  exit 0
}

## disk space

function emfa_check_disk_space() {
  # Abort update if disk space is low use is >= 95%
  THRESHOLD="95"
  DISKUSE=$(df -hP -t ext4 --local | awk '{print $5}' | grep [0-9] | tr -d "%")
  for i in $DISKUSE; do
    if [[ $i -ge $THRESHOLD ]]; then

      echo "FATAL:  Update aborted.  Low disk space <=5% detected on one"
      echo "or more local ext4 filesystems!"
      echo "Free up disk space before continuing."
      echo "Please visit http://emfabox.com for assistance."

      abort
    fi
  done

}

# get current version

function emfa_get_cversion() {
  if [ -f /etc/EMFA-Version ]
    then
      CVERSION="`head -1 /etc/EMFA-Version`"
    else
      echo "ERROR: No valid version file found on this system."
      echo "ERROR: exiting now"
      exit 0
  fi

  # Check if we run an beta version
  if [[ $CVERSION =~ ^[0-9]{1}\.[0-9]{1}\.[0-9]{1}\.[0-9]{1}-beta$ ]]
    then
      echo "ERROR: You seem to be running an beta version, no upgrade possible."
      echo "ERROR: please look at http://emfabox.com for more information."
      echo "ERROR: exiting now"
      exit 0
  fi

  # Check if CVERSION is an valid Version file
  if ! [[ $CVERSION =~ ^[0-9]{1}\.[0-9]{1}\.[0-9]{1}\.[0-9]{1}$ ]]
    then
      echo "ERROR: The version file on your system does not seem to be valid."
      echo "ERROR: exiting now"
      exit 0
  fi
}

#initialize 
function emfa_initialize() {

  mkdir -p $UPDATEDIR
  cd $UPDATEDIR
}


function emfa_user_check() {
  if [ `whoami` == root ]
    then
      echo "Good you are root"
  else
    echo "Please become root to run this update"
    exit 0
  fi
}

###
function emfa_reboot() { 

 # Write the latest version number
  echo "$VERSION" > /etc/EMFA-Version

  cd /tmp
  
  rm -f emfaupgrade.sh
  
  [[ -n $UPDATEDIR ]] && rm -rf $UPDATEDIR
  
  
#re create dir's

if [ ! -d /opt/emfa/update ] ; then
    mkdir -p /opt/emfa/update
fi
chmod 755 /opt/emfa/update
chown apache:apache /opt/emfa/update

if [ ! -d /opt/emfa/staging ] ; then
    mkdir -p /opt/emfa/staging
fi
chmod 755 /opt/emfa/staging
chown apache:apache /opt/emfa/staging

  
  
#Reboot
echo "Done - Rebooting now..."
/sbin/init 6
exit 

}


### STARTUP

function emfa_startup() {
emfa_get_cversion
if [[ $CVERSION == "1.0.0.9" ]] ; then
  emfa_initialize
  emfa_update
#  emfa_finish_update
  emfa_reboot 
else
  echo "ERROR" > /tmp/error
  emfa_abort
fi

}

####################################
# START
####################################
emfa_user_check
emfa_check_os
emfa_check_disk_space
emfa_startup
 





